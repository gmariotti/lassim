Peripheral Genes Optimization Example
=====================================

Example of the Peripheral Genes Optimization where a reduced network and core system is used. 
The Core System is composed of 12 transcription factors (COPEB, ELK1, GATA3, IRF4, JUN, MAF, MYB, 
NFATC3, NFKB1, RELA, STAT3, USF2) with the number of interactions equal to 28. The Peripheral Genes
used are 5 (A1BG, NAT1, AAMP, AANAT, AARS) with initial supposed interaction equal to 5, 1, 3, 1 and 1
respectively.

In order to run the example, two commands can be used:
```
python <repository>/lassim_toolbox/source/lassim_peripherals.py <configuration>
OR
mpirun -n <n> python <repository>/lassim_toolbox/source/lassim_peripherals_mpy.py <configuration>
```

Three configuration files are available:
- `configuration.ini` is the example for optimizations using step-removal function without
the use of perturbations data.
- `configuration_l1.ini` is the example for optimizations using the L1 penalty, with edge
removal based on a given threshold.
- `configuration_perturbations.ini` is the example for optimizations using step-removal function 
with the use of perturbations data. 