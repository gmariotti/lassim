from argparse import ArgumentParser

import numpy as np
import pandas as pd
from py2cytoscape.data.cynetwork import CyNetwork
from py2cytoscape.data.cyrest_client import CyRestClient
from py2cytoscape.data.style import Style, StyleUtil

"""
visit http://idekerlab.github.io/cyREST/ for the REST API of CyREST

use "curl http://127.0.0.1/v1/<API>" to get information about the <API> you want
to use 
"""


def create_core_dataframe(file: str, n: int) -> pd.DataFrame:
    frame = pd.read_csv(file, sep="\t", nrows=n)
    columns = frame.columns
    source = columns[len(columns) - n:].tolist()
    target = source

    values = frame[source].values
    reactions = []
    for i in range(n):
        for j in range(n):
            reactions.append([target[i], source[j], values[i, j]])
    filtered_reactions = filter(lambda x: float(x[2]) != 0.0, reactions)
    network_frame = pd.DataFrame(
        data=[val for val in filtered_reactions],
        columns=["target", "source", "weight"]
    )
    network_frame["weight"] = network_frame["weight"].apply(pd.to_numeric)
    network_frame["interaction"] = np.array(
        ["-" for _ in range(network_frame.shape[0])]
    )
    return network_frame


def create_peripherals_dataframe(file: str, n: int) -> pd.DataFrame:
    frame = pd.read_csv(file, sep="\t")
    columns = frame.columns
    source = columns[len(columns) - n:].tolist()
    target = frame["target"].tolist()

    values = frame[source].values
    reactions = []
    for i in range(values.shape[0]):
        for j in range(n):
            reactions.append([target[i], source[j], values[i, j]])
    filtered_reactions = filter(lambda x: float(x[2] != 0.0), reactions)
    network_frame = pd.DataFrame(
        data=[val for val in filtered_reactions],
        columns=["target", "source", "interaction"]
    )
    network_frame["interaction"] = network_frame["interaction"].apply(
        pd.to_numeric
    )
    return network_frame


def add_network(cy_network: CyNetwork, frame: pd.DataFrame, chunk_size: int):
    number_of_chunks = int(frame.shape[0] / chunk_size) + 1
    chunks = np.array_split(frame, number_of_chunks)

    # dictionary of type -> <name:id>
    nodes = {row["name"]: str(idx)
             for idx, row in cy_network.get_node_table().iterrows()}
    for chunk in chunks:
        edge_list = [(row["source"], row["target"], str(row["interaction"]))
                     for index, row in chunk.iterrows()]

        # adds nodes that are not present yet in the network
        nodes_to_add = set()
        for source, target, interaction in edge_list:
            if source not in nodes:
                nodes_to_add.add(source)
            if target not in nodes:
                nodes_to_add.add(target)
        if len(nodes_to_add) > 0:
            cy_network.add_nodes(list(nodes_to_add))
            nodes = {row["name"]: str(idx)
                     for idx, row in cy_network.get_node_table().iterrows()}
        edge_list_ids = [(nodes[source], nodes[target], interaction)
                         for source, target, interaction in edge_list]
        cy_network.add_edges(edge_list_ids)


def create_style(style_name: str) -> Style:
    new_style = cy.style.create(style_name)
    arrow_point = StyleUtil.create_point(0.0, "T", "T", "DELTA")
    new_style.create_continuous_mapping(
        "weight", "Double", "EDGE_TARGET_ARROW_SHAPE", arrow_point
    )
    color_point = StyleUtil.create_point(0.0, "green", "green", "red")
    new_style.create_continuous_mapping(
        "weight", "Double", "EDGE_STROKE_UNSELECTED_PAINT", color_point
    )
    new_style.create_continuous_mapping(
        "weight",
        "Double",
        "EDGE_TARGET_ARROW_UNSELECTED_PAINT",
        color_point
    )
    new_style.create_passthrough_mapping(
        column="name",
        vp="NODE_LABEL"
    )
    new_style.update_defaults({
        "NODE_FILL_COLOR": "#99CCFF"
    })
    return new_style


if __name__ == '__main__':
    script_name = "lassim_cytoscape"
    max_per_chunk = 100

    parser = ArgumentParser(script_name)
    parser.add_argument("filename",
                        help="Name of the file to visualize in Cytoscape.")
    parser.add_argument("#tfs", type=int,
                        help="# transcription factors in the Core System.")
    parser.add_argument("-t", "--type",
                        default="core", choices=["core", "peripheral"],
                        help="Type of data to visualize.")
    parser.add_argument("-n", "--name",
                        default="lassim network",
                        help="Name of the network in Cytoscape.")
    parser.add_argument("-c", "--collection",
                        default="lassim collection",
                        help="Name of the collection in Cytoscape.")
    parser.add_argument("--core", default="",
                        help="Name of the file with the core system. Used with"
                             "the peripheral genes.")
    args = parser.parse_args()

    num_tf = getattr(args, "#tfs")
    filename = getattr(args, "filename")
    file_type = getattr(args, "type")
    name = getattr(args, "name")
    collection = getattr(args, "collection")

    if file_type == "core":
        core_frame = create_core_dataframe(filename, num_tf)
        net_frame = None
    elif file_type == "peripheral":
        net_frame = create_peripherals_dataframe(filename, num_tf)
        core_name = getattr(args, "core")
        core_frame = None
        if core_name != "":
            core_frame = create_core_dataframe(core_name, num_tf)
    else:
        raise RuntimeError("{} is not a valid file type".format(file_type))

    try:
        cy = CyRestClient()
        cy.session.delete()
        if core_frame is not None:
            network = cy.network.create_from_dataframe(
                core_frame,
                name=name,
                collection=collection,
                extra_columns=["weight"]
            )
        else:
            network = cy.network.create(name=name, collection=collection)

        if net_frame is not None:
            add_network(network, net_frame, max_per_chunk)
        cy.layout.apply(network=network, name="kamada-kawai")
        cy.layout.fit(network=network)
        print("Cytoscape network created...")

        # Styling
        print("Cytoscape stiling...")
        style = create_style("LassimStyle")
        cy.style.apply(style=style, network=network)

    except Exception as e:
        if type(e).__name__ == ConnectionError.__name__:
            print("Error in connecting with Cytoscape...")
            print("Cytoscape must be running in order to interface with it.")
        print(e)
