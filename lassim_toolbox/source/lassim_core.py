import logging
from typing import Callable, NamedTuple, Iterable, List, Tuple

import numpy as np

from customs.module_loader import ModuleLoader
from utilities.data_classes import ReferenceInfo, InputExtra

try:
    import pygmo as pg
except ImportError:
    logging.getLogger(__name__).exception("Error importing pygmo")
    exit(1)

try:
    from lassim.base_solution import BaseSolution
    from lassim.handlers.composite_handler import CompositeSolutionsHandler
    from lassim.handlers.csv_handlers import SimpleCSVSolutionsHandler, \
        DirectoryCSVSolutionsHandler
    from lassim.handlers.plot_handler import PlotBestSolutionsHandler
    from lassim.lassim_context import LassimContext, ReferencesModule
    from lassim.lassim_exception import LassimException
    from lassim.solutions_handler import SolutionsHandler
    from lassim.type_aliases import Tuple4V
except ImportError:
    logging.getLogger(__name__).exception("Error importing lassim")
    exit(1)

from sortedcontainers import SortedDict

from customs.configuration_custom import parse_core_config, default_terminal, \
    core_configuration_example, parse_modules_config, sections, \
    parse_optimization_config, parse_core_extra_config
from customs.core_creation import create_core, problem_setup, optimization_setup

"""
Main script for handling the lassim_api problem in the Lassim Toolbox.
Can also be used as an example on how the toolbox works and how the lassim_api
module can be integrated into an existing pipeline.
"""

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.1.0"


def data_producer(context: LassimContext, data_tuple: NamedTuple
                  ) -> Callable[[BaseSolution], Iterable[Tuple4V]]:
    ode_func = context.references.ode_function
    y0 = data_tuple.y0
    time = data_tuple.times
    data = data_tuple.data
    result = np.empty(y0.size)
    res_time = np.linspace(time[0], time[time.size - 1], num=1000)

    def wrapper(solution: BaseSolution) -> Iterable[Tuple4V]:
        results = ode_func(
            y0, res_time, solution.solution_vector, solution.react_vect,
            solution.react_mask, y0.size, result
        )
        norm_results = results / np.amax(results, axis=0)
        for i in range(0, y0.size):
            yield data[:, i], time, norm_results[:, i], res_time

    return wrapper


def load_modules(modules: List[ReferenceInfo], extra: InputExtra
                 ) -> ReferencesModule:
    references = {
        "ode_fun": None,
        "pert_fun": None,
        "iter_fun": None,
        "solution_class": None
    }
    for mod in modules:
        reference = ModuleLoader.load_reference(
            mod.module_name, mod.reference_name, mod.module_path
        )
        if mod.type == sections.ode:
            references["ode_fun"] = reference
        elif mod.type == sections.perturbations:
            references["pert_fun"] = reference
        elif mod.type == sections.iteration:
            references["iter_fun"] = reference
        elif mod.type == sections.solution_class:
            references["solution_class"] = reference
        else:
            raise LassimException("Type {} does not exist!".format(mod.type))

    if references["ode_fun"] is None:
        logging.getLogger(__name__).error("Missing ODE Function!")
        raise LassimException("Missing ODE Function!")
    if references["iter_fun"] is None:
        logging.getLogger(__name__).error("Missing Iteration Function!")
        raise LassimException("Missing Iteration Function!")
    if references["solution_class"] is None:
        logging.getLogger(__name__).error("Missing SolutionClass!")
        raise LassimException("Missing SolutionClass!")

    references["iter_fun"] = references["iter_fun"](extra)

    return ReferencesModule(**references)


def core_handlers(context: LassimContext, data: NamedTuple, output: NamedTuple
                  ) -> Tuple[SolutionsHandler, DirectoryCSVSolutionsHandler]:
    # list of headers that will be used in each solution
    core = context.network.core
    tfacts = [tfact for tfact in core.tfacts]
    headers = context.references.solution_class.solution_headers() + tfacts
    csv_handler = SimpleCSVSolutionsHandler(
        output.directory, output.num_solutions, headers
    )
    axis = [("time", "[{}]".format(name)) for name in tfacts]
    plot_handler = PlotBestSolutionsHandler(
        output.directory, tfacts, axis, data_producer(context, data)
    )
    handler = CompositeSolutionsHandler([csv_handler, plot_handler])

    final_handler = DirectoryCSVSolutionsHandler(
        output.directory, float("inf"), headers
    )
    return handler, final_handler


def lassim_core():
    script_name = "lassim_core"

    try:
        # arguments from terminal are parsed
        args = default_terminal(script_name, core_configuration_example)
        files, output, is_pert = parse_core_config(args.configuration)
        main_args, sec_args = parse_optimization_config(
            args.configuration, is_pert
        )
        modules = parse_modules_config(args.configuration)
        extra = parse_core_extra_config(args.configuration)
        references = load_modules(modules, extra)
        core = create_core(files.network)
        # creates a context for solving this problem
        context = LassimContext(
            core, references, main_args, sec_args
        )
        # returns a namedtuple with the data parsed and the factory for the
        # problem construction
        data, p_factory = problem_setup(files, context)
        base_builder, start_problem = optimization_setup(context, p_factory)

        # construct the solutions handlers for managing the solution of each
        # optimization step
        handler, final_handler = core_handlers(context, data, output)
        # building of the optimization based on the parameters passed as
        # arguments
        optimization = base_builder.build(
            context, p_factory, start_problem,
            SortedDict(core.from_reactions_to_ids()), handler,
        )
        # list of solutions from solving the problem
        solutions = optimization.solve()
        final_handler.handle_solutions(solutions, dirname="best_solutions")
        # FIXME - save best solution by name
    except LassimException as e:
        logging.getLogger(__name__).error(e)
    except SystemExit as e:
        logging.getLogger(__name__).warning("Exit Code {}".format(e))
    except:
        logging.getLogger(__name__).exception("An exception occurred")


if __name__ == "__main__":
    lassim_core()
