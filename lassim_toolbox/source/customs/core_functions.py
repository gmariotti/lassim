import logging
from copy import deepcopy
from typing import Tuple, Optional, Callable, List

import numpy as np
from lassim.lassim_context import LassimContext
from lassim.lassim_exception import LassimException
from lassim.problems.core_problem import CoreProblemFactory, CoreProblem
from lassim.solutions.lassim_solution import CoreSolution
from lassim.type_aliases import Vector, Float
from sortedcontainers import SortedDict, SortedList

from utilities.data_classes import InputExtra

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.1.0"

"""
Set of custom functions for the handling the optimization problem of a core 
system.
Can be considered an example on how to integrate the lassim API in an existing
pipeline.
"""


def default_bounds(num_tfacts: int, num_react: int) -> Tuple[Vector, Vector]:
    """
    Creates a tuple containing as first element an array with the lower bounds, 
    and as the second element the array of upper bounds for the parameters to 
    optimize. By default, for lambda/vmax the bounds are (0, 20), while for the 
    k are (-20, 20)

    :param num_tfacts: Number of transcription factors in the network.
    :param num_react: Number of reactions between the transcription factors.
    :return: Tuple containing the lower bounds and upper bounds as numpy arrays.
    """

    lower_bounds = [0.0 for _ in range(num_tfacts * 2)]
    upper_bounds = [20.0 for _ in range(num_tfacts * 2 + num_react)]
    for _ in range(num_react):
        lower_bounds.append(-20.0)
    return np.array(lower_bounds), np.array(upper_bounds)


def generate_reactions_vector(reactions: SortedDict) -> Tuple[Vector, Vector]:
    """
    From a reactions map, generates the corresponding numpy vector for
    reactions and its boolean mask. The reaction vector contains #tfacts^2
    elements, and each #tfacts subset represent the list of reactions with
    other transcription factors for one of them. The values are 0 if the
    reaction is not present and numpy.inf if it is.

    :param reactions: Map of transcription factors and their reactions
    :return: ndarray for reactions and its corresponding boolean mask
    """

    reacts = []
    num_tfacts = len(reactions.keys())
    for tfact, reactions in reactions.items():
        vector = np.zeros(num_tfacts, dtype=Float)
        vector[reactions] = np.inf
        reacts.append(vector.tolist())
    reacts_flatten = [val for sublist in reacts for val in sublist]
    np_reacts = np.array(reacts_flatten, dtype=Float)

    return np_reacts, np_reacts == np.inf


def _remove_reaction_from_index(index: int,
                                vector: Vector,
                                reactions: SortedDict,
                                diff_from_start: int = 0
                                ) -> Tuple[Vector, SortedDict, Tuple[int, int]]:
    # for each transcription factors and its reactions, count is increased
    # until min_index is found, in order to find each reaction to remove. The
    # main reaction map is not modified because I'm working on the id one
    # that is dynamically built every time
    reactions = deepcopy(reactions)
    count = 0
    for key, reacts_list in reactions.iteritems():
        count += len(reacts_list)
        if count > index:
            # this transcription factor is the one containing the reaction to
            # remove
            react_index = index - count + len(reacts_list)
            value = reacts_list.pop(react_index)
            vector = np.delete(vector, index + diff_from_start)
            return vector, reactions, (value, key)
    raise LassimException("Index {} to remove not found!!".format(index))


def remove_lowest_reaction(decision_vector: Vector,
                           reactions: SortedDict
                           ) -> Tuple[Vector, SortedDict, Tuple[int, int]]:
    """
    From the decision vector, removes the reaction with the lowest |val|. 
    Moreover, removes the corresponding reaction from the reaction dictionary.

    :param decision_vector: The vector containing the optimization parameters.
    :param reactions: Map of reactions by ids.
    :return: Decision vector without the lowest |react| and the new reactions
        dictionary.
    """

    tfacts_num = len(reactions.keys())
    k_values = decision_vector[tfacts_num * 2:]
    abs_k = np.absolute(k_values)
    min_index = np.argmin(abs_k)
    if min_index.size > 1:
        min_index = min_index[0]

    return _remove_reaction_from_index(
        min_index, decision_vector, reactions, diff_from_start=tfacts_num * 2
    )


def remove_under_threshold_reactions(decision_vector: Vector,
                                     reactions: SortedDict,
                                     perc_threshold: float
                                     ) -> Tuple[Vector,
                                                SortedDict,
                                                List[Tuple[int, int]]]:
    """
    From the decision vector, removes all the reactions with their |val| lower
    than the percentage threshold of the greatest reaction. 
    Moreover, removes the corresponding reactions from the reaction dictionary.

    :param decision_vector: The vector containing the optimization parameters.
    :param reactions: Map of reactions by ids.
    :param perc_threshold: Percentage of the maximum value to use as threshold.
        Should be a value between 0 and 1.
    :return: Decision vector without the reactions under the threshold and the 
        new reactions dictionary. 
    """

    num_tfacts = len(reactions.keys())
    k_vector = decision_vector[num_tfacts * 2:]
    k_absolute = np.absolute(k_vector)
    max_value = np.max(k_absolute)
    threshold = max_value * perc_threshold
    indexes = [ix
               for ix, condition in enumerate(k_absolute <= threshold)
               if condition]
    removed_reactions = []
    while len(indexes) > 0:
        decision_vector, reactions, rem_react = _remove_reaction_from_index(
            indexes[0], decision_vector, reactions,
            diff_from_start=num_tfacts * 2
        )
        removed_reactions.append(rem_react)
        k_vector = decision_vector[num_tfacts * 2:]
        k_absolute = np.absolute(k_vector)
        indexes = [ix
                   for ix, condition in enumerate(k_absolute <= threshold)
                   if condition]
    return decision_vector, reactions, removed_reactions


def iter_function(extra: InputExtra) -> Callable:
    # extra is not used in this case, but it can be useful for other iteration
    # functions initialization
    def iter_wrapper(factory: CoreProblemFactory, context: LassimContext,
                     solutions: SortedList, last_solution: CoreSolution,
                     prob_init: bool = False
                     ) -> Tuple[Optional[CoreProblem], SortedDict, bool]:
        """
        Custom function for performing an iteration after a completed 
        optimization for a certain number of variables in the core system. This 
        function generates a new problem to solve, with a variable less, a new 
        dictionary of the current reactions in the core system and a boolean 
        value to determine if the optimization should continue or not. The 
        reaction removed from the old problem is the one with the lowest 
        absolute value.
    
        :param factory: Instance of CoreProblemFactory for building a new 
            instance of CoreProblem/CoreWithPerturbationsProblem.
        :param context: The current working LassimContext.
        :param solutions: List of CoreSolution sorted by cost function value.
        :param last_solution: Latest solution generated.
        :param prob_init: Boolean value for using the function for the problem 
            initialization. Default value is False.
        :return: If a new iteration can be performed, it returns the new 
            problem, its reactions dictionary and True. Otherwise returns None, 
            an empty dict and False.
        """

        core = context.network.core
        if prob_init:
            reactions_ids = SortedDict(core.from_reactions_to_ids())
            new_problem = factory.build(
                dim=(core.num_tfacts * 2 + core.react_count),
                bounds=default_bounds(core.num_tfacts, core.react_count),
                vector_map=generate_reactions_vector(reactions_ids),
                type="classic"
            )
            return new_problem, reactions_ids, False
        else:
            react_mask = last_solution.react_mask
            # checks how many true are still present in the reaction mask.
            # If > 0, it means that there's still at least a reaction
            if react_mask[react_mask].size > 0:
                reactions = last_solution.reactions_ids
                reduced_vect, new_reactions, rem_react = remove_lowest_reaction(
                    last_solution.solution_vector, reactions
                )
                tfact = core.get_tfact_name(rem_react[0])
                reaction = core.get_tfact_name(rem_react[1])
                logging.getLogger(__name__).info(
                    "Removed reaction from {} to {}".format(reaction, tfact)
                )
                new_react, new_mask = generate_reactions_vector(new_reactions)
                num_react = new_react[new_mask].size
                new_problem = factory.build(
                    dim=len(new_reactions.keys()) * 2 + num_react,
                    bounds=default_bounds(len(new_reactions.keys()), num_react),
                    vector_map=(new_react, new_mask),
                    known_sol=[reduced_vect],
                    type="classic"
                )
                return new_problem, new_reactions, True
            else:
                return None, SortedDict(), False

    return iter_wrapper


def _get_l1_arguments(extra_args):
    try:
        lambda_val = float(extra_args["lambda"])
        step_val = float(extra_args["step"])
        stop_val = float(extra_args["stop"])
        repetitions = int(extra_args.get("repetitions", 0))

        if lambda_val >= stop_val:
            raise LassimException("lambda must be lower than stop!!")
        if step_val <= 0:
            raise LassimException("step must be greater than 0!!")
        if repetitions < 0:
            raise LassimException("repetitions must be non negative!!")
    except ValueError as e:
        logging.getLogger(__name__).exception("Error converting to float!!")
        raise LassimException(e)
    logging.getLogger(__name__).info(
        "(lambda = {}, step = {}, stop = {}, repetitions = {})".format(
            lambda_val, step_val, stop_val, repetitions
        ))
    return lambda_val, step_val, stop_val, repetitions


def _get_lambdas(lambda_val: float,
                 stop_val: float,
                 step_val: float,
                 repetitions: int) -> List[float]:
    lambdas = np.arange(start=lambda_val, stop=stop_val, step=step_val).tolist()
    lambdas = [value
               for lambdas_copy in [lambdas.copy()
                                    for _ in range(repetitions + 1)]
               for value in lambdas_copy]
    return lambdas


def iter_l1(extra: InputExtra) -> Callable:
    logger = logging.getLogger(__name__)
    extra_args = extra.user_dictionary
    lambda_val, step_val, stop_val, repetitions = _get_l1_arguments(extra_args)
    try:
        perc_threshold = float(extra_args["threshold"])
        if 0.0 >= perc_threshold or perc_threshold > 1.0:
            raise LassimException(
                "Percentage threshold must be greater than 0 and less than 1"
            )
    except ValueError as e:
        logger.exception("Error converting to float")
        raise LassimException(e)

    logger.info("Percentage threshold set to {}".format(perc_threshold))
    lambdas = _get_lambdas(lambda_val, stop_val, step_val, repetitions)
    logger.info(lambdas)

    # steps for lambda, when to stop, problem creation without edge removal
    def iter_wrapper(factory: CoreProblemFactory, context: LassimContext,
                     solutions: SortedList, last_solution: CoreSolution,
                     prob_init: bool = False
                     ) -> Tuple[Optional[CoreProblem], SortedDict, bool]:
        core = context.network.core
        if prob_init:
            reactions_ids = SortedDict(core.from_reactions_to_ids())
            new_lambda = lambdas.pop(0)
            new_problem = factory.build(
                dim=(core.num_tfacts * 2 + core.react_count),
                bounds=default_bounds(core.num_tfacts, core.react_count),
                vector_map=generate_reactions_vector(reactions_ids),
                type="L1",
                lambdas=new_lambda
            )
            logger.info(
                "Value of lambda is {}".format(new_lambda)
            )
            return new_problem, reactions_ids, False
        else:
            if len(lambdas) > 0:
                new_lambda = lambdas.pop(0)
                logger.info(
                    "Value of lambda is {}".format(new_lambda)
                )
                reactions = last_solution.reactions_ids
                red_vect, new_reacts, rem_reacts = \
                    remove_under_threshold_reactions(
                        last_solution.solution_vector,
                        reactions,
                        perc_threshold
                    )
                for removed in rem_reacts:
                    tfact = core.get_tfact_name(removed[0])
                    reaction = core.get_tfact_name(removed[1])
                    logger.info(
                        "Removed reaction from {} to {}".format(
                            reaction, tfact
                        ))
                new_react, new_mask = generate_reactions_vector(new_reacts)
                num_react = new_react[new_mask].size

                known_solutions = [red_vect]

                dimension = len(new_reacts.keys()) * 2 + num_react
                new_problem = factory.build(
                    dim=dimension,
                    bounds=default_bounds(len(new_reacts.keys()), num_react),
                    vector_map=(new_react, new_mask),
                    known_sol=known_solutions,
                    type="L1",
                    lambdas=new_lambda
                )
                return new_problem, new_reacts, True
            else:
                return None, SortedDict(), False

    return iter_wrapper


def iter_l1_naive(extra: InputExtra) -> Callable:
    extra_args = extra.user_dictionary
    lambda_val, step_val, stop_val, repetitions = _get_l1_arguments(extra_args)
    lambdas = _get_lambdas(lambda_val, stop_val, step_val, repetitions)
    logging.getLogger(__name__).info(lambdas)

    # steps for lambda, when to stop, problem creation without edge removal
    def iter_wrapper(factory: CoreProblemFactory, context: LassimContext,
                     solutions: SortedList, last_solution: CoreSolution,
                     prob_init: bool = False
                     ) -> Tuple[Optional[CoreProblem], SortedDict, bool]:
        if prob_init:
            core = context.network.core
            reactions_ids = SortedDict(core.from_reactions_to_ids())
            new_lambda = lambdas.pop(0)
            new_problem = factory.build(
                dim=(core.num_tfacts * 2 + core.react_count),
                bounds=default_bounds(core.num_tfacts, core.react_count),
                vector_map=generate_reactions_vector(reactions_ids),
                type="L1",
                lambdas=new_lambda
            )
            logging.getLogger(__name__).info(
                "Value of lambda is {}".format(new_lambda)
            )
            return new_problem, reactions_ids, False
        else:
            if len(lambdas) > 0:
                new_lambda = lambdas.pop(0)
                logging.getLogger(__name__).info(
                    "Value of lambda is {}".format(new_lambda)
                )
                reactions = last_solution.reactions_ids
                new_react, new_mask = generate_reactions_vector(reactions)
                num_react = new_react[new_mask].size
                known_solutions = [
                    solution.solution_vector
                    for solution in solutions
                ]

                new_problem = factory.build(
                    dim=len(reactions.keys()) * 2 + num_react,
                    bounds=default_bounds(len(reactions.keys()), num_react),
                    vector_map=(new_react, new_mask),
                    known_sol=known_solutions,
                    type="L1",
                    lambdas=new_lambda
                )
                return new_problem, reactions, True
            else:
                return None, SortedDict(), False

    return iter_wrapper
