import logging
from copy import deepcopy
from typing import Tuple, Callable, Optional, List

import numpy as np
from lassim.lassim_context import LassimContext
from lassim.lassim_exception import LassimException
from lassim.problems.network_problem import NetworkProblemFactory, \
    NetworkProblem
from lassim.solutions.lassim_solution import PeripheralSolution
from lassim.type_aliases import Vector, Tuple2V, Float
from sortedcontainers import SortedDict, SortedSet, SortedList

from utilities.data_classes import PeripheralWithCoreData, InputExtra

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.1.0"

"""
Set of custom functions for peripherals problem creation and iteration.
"""


def default_bounds(num_genes: int, num_react: int) -> Tuple[Vector, Vector]:
    """
    Creates a tuple containing as first element the vector of lower bounds, and
    as second element the vector of upper bounds for the parameters to optimize.
    By default, for lambda/vmax the bounds are (0, 20), while for the k are
    (-20, 20)

    :param num_genes: Number of genes in the network.
    :param num_react: Number of reactions between genes and transcription
        factors of the core system.
    :return: Tuple containing the lower bounds and upper bounds as vectors.
    """

    lower_bounds = [0.0 for _ in range(0, num_genes * 2)]
    upper_bounds = [20.0 for _ in range(0, num_genes * 2 + num_react)]
    for _ in range(0, num_react):
        lower_bounds.append(-20.0)
    return np.array(lower_bounds), np.array(upper_bounds)


def _remove_reaction_from_index(min_index: int,
                                vector: Vector,
                                reactions: SortedSet
                                ) -> Tuple[Vector, SortedSet, int]:
    try:
        react = reactions.pop(min_index)
        index_to_remove = min_index + 2
        vector = np.delete(vector, index_to_remove)
        return vector, reactions, react
    except (IndexError, KeyError):
        raise LassimException(
            "Index {} to remove not found!!".format(min_index))


def remove_lowest_reaction(solution_vector: Vector,
                           reactions: SortedSet,
                           ) -> Tuple[Vector, SortedSet, int]:
    """
    From a solution vector removes the reaction with the lowest absolute value
    and generates a new dictionary with all the reactions except the one
    removed.

    :param solution_vector: The numpy.ndarray representing an optimization
        solution.
    :param reactions: The set of reactions for a gene.
    :return: Tuple containing the solution_vector and the reactions set
        without the reaction removed.
    """

    absolute_k = np.absolute(solution_vector[2:])
    min_index = np.argmin(absolute_k)
    if min_index.size > 1:
        min_index = min_index[0]
    reactions = deepcopy(reactions)

    return _remove_reaction_from_index(min_index, solution_vector, reactions)


def remove_under_threshold_reactions(decision_vector: Vector,
                                     reactions: SortedSet,
                                     perc_threshold: float,
                                     ) -> Tuple[Vector, SortedSet, List[int]]:
    """
    From the decision vector, removes all the reactions with their |val| lower
    than the percentage threshold of the greatest reaction.
    Moreover, removes the corresponding reactions from the reaction set.

    :param decision_vector: The vector containing the optimization parameters.
    :param reactions: Set of reactions for gene.
    :param perc_threshold: Percentage of the maximum value to use as threshold.
        Should be a value between 0 and 1.
    :return: Decision vector without the reactions under the threshold, and the
        new reactions set.
    """

    k_vector = decision_vector[2:]
    k_absolute = np.absolute(k_vector)
    max_value = np.max(k_absolute)
    threshold = max_value * perc_threshold
    indexes = [ix
               for ix, condition in enumerate(k_absolute <= threshold)
               if condition]
    removed_reactions = []
    reactions = deepcopy(reactions)
    while len(indexes) > 0:
        decision_vector, reactions, rem_react = _remove_reaction_from_index(
            indexes[0], decision_vector, reactions
        )
        removed_reactions.append(rem_react)
        k_vector = decision_vector[2:]
        k_absolute = np.absolute(k_vector)
        indexes = [ix
                   for ix, condition in enumerate(k_absolute <= threshold)
                   if condition]
    return decision_vector, reactions, removed_reactions


def generate_core_vector(core_data: Vector,
                         num_tf: int,
                         genes_reactions: SortedSet) -> Tuple[Vector, Vector]:
    """
    Used for generating a core system vector needed in NetworkProblem instances. 
    Core reactions not present.

    :param core_data: Represents the data for the core system. It must be a 2D
        matrix, with each row representing the data for a transcription factor.
        Each row must be in the format: [lambda, vmax, k1,...,kn]
    :param num_tf: Number of transcription factors in the core system.
    :param genes_reactions: Dictionary containing the reactions between the
        genes and the core system.
    :return: Tuple containing a vector in the format:
        [lambda_1,..,lambda_#tf, lambda_gene,
        vmax_1,..,vmax_#tf,vmax_gene,
        react_gene1,..,react_gene#tf] and its boolean mask for discriminating
        the genes data. Each data related to the core system is the one from the
        core_data, while the data related to the gene are all set to numpy.inf.
        The mask represents which data are numpy.inf and which are not.
    """

    # is the same value for vmax
    tot_lambdas = num_tf + 1
    # the peripheral can have max num_tf reactions
    num_genes_reactions = len(genes_reactions)
    core_vector = np.zeros(tot_lambdas * 2 + num_genes_reactions)

    # changes the values of lambdas and vmax with the one in the lassim_api data
    core_vector[:num_tf] = core_data[:, 0]
    core_vector[num_tf + 1: num_tf * 2 + 1] = core_data[:, 1]

    # changes the values for the genes data to 0
    # in order lambda, vmax and num_tf*reactions
    core_vector[num_tf: num_tf + 1] = np.inf
    core_vector[num_tf * 2 + 1: num_tf * 2 + 2] = np.inf
    core_vector[num_tf * 2 + 2:] = np.inf

    return core_vector, core_vector == np.inf


def generate_reactions_vector(gene_reactions: SortedSet,
                              core_data: Vector,
                              num_tf: int, dt_react=Float) -> Tuple2V:
    """
    From a reactions map and the core data, generates the corresponding numpy
    vector for the reactions of core and genes and its boolean mask. The
    reaction vector contains #tfacts^2 + #tfacts*#genes elements, and each
    #tfacts subset represent the list of reactions with the transcription
    factors. The values are 0 if the reaction is not present and numpy.inf
    if it is.

    :param gene_reactions: Set of reactions for a gene in respect to the core 
        system.
    :param core_data: Represents the data for the core system. It must be a 2D 
        matrix, with each row representing the data for a transcription factor.
        Each row must be in the format: [lambda, vmax, k1,...,kn]
    :param num_tf: Number of transcription factors in the core system.
    :param dt_react: The type of numpy array you want to build.
    :return: Tuple containing the reaction vector and its corresponding mask.
    """

    reacts = []
    vector = np.zeros(num_tf + 1, dtype=dt_react)
    vector[gene_reactions] = np.inf
    reacts.append(vector.tolist())
    # adds an extra reaction set to 0 in each row
    core_data = np.insert(core_data, num_tf + 2, 0, axis=1)
    core_reacts_flatten = [val for row in core_data
                           for val in row[2:]]
    np_reacts = np.array(
        core_reacts_flatten + vector.tolist(), dtype=dt_react
    )

    return np_reacts, np_reacts == np.inf


def iter_function(extra: InputExtra) -> Callable:
    def iter_wrapper(data: PeripheralWithCoreData
                     ) -> Callable[[NetworkProblemFactory, LassimContext,
                                    SortedList, PeripheralSolution, bool],
                                   Tuple[Optional[NetworkProblem],
                                         SortedDict, bool]]:
        """
        This function returns a custom function for performing an iteration after
        a completed optimization for a certain number of variables in the network.
        This function generates a new problem to solve, with one less variable, a
        new dictionary of reactions and a boolean value in case the optimization
        should continue or not. The reaction removed from the old problem is the
        one with the lowest absolute value.
    
        :param data: Current peripheral data containing also the data for the core
            system.
        :return: Iteration function that accept as input a NetworkProblemFactory, a 
            LassimContext, the list of solutions found so far and the 
            PeripheralSolution to evaluate.
        """

        core_data = data.core_data

        def wrapper(factory: NetworkProblemFactory,
                    context: LassimContext,
                    solutions: SortedList,
                    last_solution: PeripheralSolution,
                    prob_init: bool = False
                    ) -> Tuple[Optional[NetworkProblem], SortedDict, bool]:
            num_tf = context.network.core.num_tfacts
            if prob_init:
                start_problem = factory.build(
                    dim=(2 + data.num_react),
                    bounds=default_bounds(1, data.num_react),
                    vector_map=generate_reactions_vector(
                        data.reactions, data.core_data, num_tf
                    ), core_data=generate_core_vector(
                        data.core_data, num_tf, data.reactions
                    ))

                return start_problem, SortedDict(), False
            else:
                react_mask = last_solution.react_mask
                if react_mask[react_mask].size > 0:
                    reactions = last_solution.reactions_ids
                    red_vect, new_reacts, rem_react = remove_lowest_reaction(
                        last_solution.solution_vector,
                        reactions[last_solution.gene_name]
                    )
                    tfact_name = context.network.core.get_tfact_name(rem_react)
                    logging.getLogger(__name__).info(
                        "Removed {} from {}".format(
                            tfact_name, last_solution.gene_name
                        ))

                    new_core_vector, new_core_mask = generate_core_vector(
                        core_data, num_tf, new_reacts
                    )
                    new_react, new_mask = generate_reactions_vector(
                        new_reacts, core_data, num_tf
                    )
                    num_genes_reactions = len(new_reacts)

                    new_problem = factory.build(
                        dim=(2 + num_genes_reactions),
                        bounds=default_bounds(1, num_genes_reactions),
                        vector_map=(new_react, new_mask),
                        known_sol=[red_vect],
                        core_data=(new_core_vector, new_core_mask)
                    )
                    gene_reactions_dict = SortedDict({
                        last_solution.gene_name: new_reacts
                    })
                    return new_problem, gene_reactions_dict, True
                else:
                    return None, SortedDict(), False

        return wrapper

    return iter_wrapper


def _get_l1_arguments(extra_args: InputExtra):
    try:
        lambda_val = float(extra_args["lambda"])
        step_val = float(extra_args["step"])
        stop_val = float(extra_args["stop"])
        repetitions = int(extra_args.get("repetitions", 0))

        if lambda_val >= stop_val:
            raise LassimException("lambda must be lower than stop!!")
        if step_val <= 0:
            raise LassimException("step must be greater than 0!!")
        if repetitions < 0:
            raise LassimException("repetitions must be non negative!!")
    except ValueError as e:
        logging.getLogger(__name__).exception("Error converting to float!!")
        raise LassimException(e)
    logging.getLogger(__name__).info(
        "(lambda = {}, step = {}, stop = {}, repetitions = {})".format(
            lambda_val, step_val, stop_val, repetitions
        ))
    return lambda_val, step_val, stop_val, repetitions


def _get_lambdas(lambda_val: float,
                 stop_val: float,
                 step_val: float,
                 repetitions: int) -> List[float]:
    lambdas = np.arange(start=lambda_val, stop=stop_val, step=step_val).tolist()
    lambdas = [value
               for lambdas_copy in [lambdas.copy()
                                    for _ in range(repetitions + 1)]
               for value in lambdas_copy]
    return lambdas


def iter_l1(extra: InputExtra) -> Callable:
    extra_args = extra.user_dictionary
    lambda_val, step_val, stop_val, repetitions = _get_l1_arguments(extra_args)
    try:
        perc_threshold = float(extra_args["threshold"])
        if 0.0 >= perc_threshold or perc_threshold > 1.0:
            raise LassimException(
                "Percentage threshold must be greater than 0 and less than 1"
            )
    except ValueError as e:
        logging.getLogger(__name__).exception("Error converting to float")
        raise LassimException(e)

    logging.getLogger(__name__).info(
        "Percentage threshold set to {}".format(perc_threshold)
    )

    def iter_wrapper(data: PeripheralWithCoreData
                     ) -> Callable[[NetworkProblemFactory, LassimContext,
                                    SortedList, PeripheralSolution, bool],
                                   Tuple[Optional[NetworkProblem],
                                         SortedDict, bool]]:
        lambdas = _get_lambdas(lambda_val, stop_val, step_val, repetitions)
        logging.getLogger(__name__).info(lambdas)
        core_data = data.core_data

        def wrapper(factory: NetworkProblemFactory,
                    context: LassimContext,
                    solutions: SortedList,
                    last_solution: PeripheralSolution,
                    prob_init: bool = False
                    ) -> Tuple[Optional[NetworkProblem], SortedDict, bool]:
            num_tf = context.network.core.num_tfacts
            logger = logging.getLogger(__name__)
            if prob_init:
                new_lambda = lambdas.pop(0)
                start_problem = factory.build(
                    dim=(2 + data.num_react),
                    bounds=default_bounds(1, data.num_react),
                    vector_map=generate_reactions_vector(
                        data.reactions, data.core_data, num_tf
                    ), core_data=generate_core_vector(
                        data.core_data, num_tf, data.reactions
                    ), type="L1",
                    lambdas=new_lambda
                )
                logger.info("Value of lambda is {}".format(new_lambda))

                return start_problem, SortedDict(), False
            else:
                if len(lambdas) > 0:
                    new_lambda = lambdas.pop(0)
                    logger.info("Value of lambda is {}".format(new_lambda))

                    reactions = last_solution.reactions_ids
                    red_vect, new_reacts, rem_react = \
                        remove_under_threshold_reactions(
                            last_solution.solution_vector,
                            reactions[last_solution.gene_name],
                            perc_threshold
                        )
                    for removed in rem_react:
                        tfact = context.network.core.get_tfact_name(removed)
                        logger.info(
                            "Removed {} from {}".format(
                                tfact, last_solution.gene_name
                            ))

                    new_core_vector, new_core_mask = generate_core_vector(
                        core_data, num_tf, new_reacts
                    )
                    new_react, new_mask = generate_reactions_vector(
                        new_reacts, core_data, num_tf
                    )
                    num_genes_reactions = len(new_reacts)

                    new_problem = factory.build(
                        dim=(2 + num_genes_reactions),
                        bounds=default_bounds(1, num_genes_reactions),
                        vector_map=(new_react, new_mask),
                        known_sol=[red_vect],
                        core_data=(new_core_vector, new_core_mask),
                        type="L1",
                        lambdas=new_lambda
                    )
                    gene_reactions_dict = SortedDict({
                        last_solution.gene_name: new_reacts
                    })
                    return new_problem, gene_reactions_dict, True
                else:
                    return None, SortedDict(), False

        return wrapper

    return iter_wrapper
