HOWTO - Custom ODE Function
===========================

One of the possible functions that you can define in **LASSIM** is the **ODE Function**. This function represents
the ODE model used to define our system, and its responsability is to solve it based on the decision vector that
will be generated at each iteration step of an optimization problem.

Below, it is possible to see the signature of the function that must be respected in order to use it inside **LASSIM**.

```python
def ode_function(y0: numpy.ndarray,
                 t: numpy.ndarray,
                 sol_vector: numpy.ndarray,
                 vect_map: numpy.ndarray,
                 vect_map_mask: numpy.ndarray,
                 size: int,
                 result: numpy.ndarray) -> numpy.ndarray:
    # perform manipulation of sol_vector based on k_map and k_map_mask
    # ode_solution must be a numpy.ndarray of shape (t.size, size)
    return ode_solution
```

The list of parameters is defined as follow:
- `y0` is a numpy array containing the starting value of each ODE.
- `t` is a numpy array containing the time points in which evaluating the ODE system.
- `sol_vector` is a numpy array containing the values of the variables to optimize at a certain iteration step of
the optimization process.
- `vect_map` and `vect_map_mask` are numpy array with the same shape, generated in the [iteration function](howto_custom_iteration.md).
These object should be used in order to identify inside the `sol_vector` the different kind of variables to optimize.
- `size` is the size of the vector `y0`.
- `result` is a numpy array of the same size of `y0` that can be used for performance reasons.

The returning value of the function **must be** a numpy array with the shape (`t.size`, `size`).

In the example below, the function `odeint1e8_lassim` represent the **LASSIM** ODE model. It uses the function
`odeint` from the package `scipy.integrate` in order to solve the ODE model.

```python
def odeint1e8_lassim(y0: numpy.ndarray,
                     t: numpy.ndarray,
                     sol_vector: numpy.ndarray,
                     k_map: numpy.ndarray,
                     k_map_mask: numpy.ndarray,
                     size: int,
                     result: numpy.ndarray) -> numpy.ndarray:
    ones = np.ones(size)

    degradation = sol_vector[:size]
    vmax = sol_vector[size: 2 * size]
    k_values = sol_vector[2 * size:]

    # map is a vector, but will be reshaped as a matrix size x size
    k_map[k_map_mask] = k_values
    return integrate.odeint(
        lassim_function, y0, t,
        args=(degradation, vmax, np.reshape(k_map, (size, size)), ones, result),
        mxstep=int(1e8)
    )
    
def lassim_function(y: numpy.ndarray,
                    t: float,
                    degradation: numpy.ndarray,
                    vmax: numpy.ndarray,
                    k_map: numpy.ndarray,
                    ones: numpy.ndarray,
                    result: numpy.ndarray) -> numpy.ndarray:
    np.add(
        np.multiply(np.negative(degradation), y, out=result),
        np.divide(vmax, np.add(ones, np.exp(np.negative(np.dot(k_map, y))))),
        out=result
    )
    return result
```