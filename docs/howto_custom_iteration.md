HOWTO - Custom Iteration Function
=================================

One of the possible functions that you can define in **LASSIM** is the **Iteration Function**. This function is
responsible for two different tasks:
- The problem initialization.
- Allowing multiple iterations between optimization processes.

Below, it is possible to see a template of a common iteration function for the **Core System Optimization**.

```python
def iteration_function(extra: InputExtra) -> Callable:
    # perform initialization operation based on extra data
    def wrapper(factory: LassimProblemFactory,
                context: LassimContext,
                solutions: SortedList,
                last_solution: BaseSolution,
                prob_init: bool = False
                ) -> Tuple[Optional[LassimProblem], SortedDict, bool]:
        if prob_init:
            # initialization of the starting problem to solve
            return start_problem, reactions, False
        else:
            # subsequent iterations
            if perform_a_new_iteration:
                return new_problem, reactions, True
            else:
                return None, SortedDict(), False
    
    return wrapper
```

The `extra` parameter allows to define a custom behaviour for the initialization and/or subsequent calls to
the iteration function. The `wrapper` is the function that is going to be called in the problem initialization
and between optimization processes. Its task is to return a `LassimProblem` to solve, the `SortedDict` representing
its reactions and a `bool` that indicates if a new optimization process should be performed or not. Other than the
variables generated by `extra`, the function parameters are:
- `LassimProblemFactory` for the construction of a new `LassimProblem`.
- `LassimContext` for the current working context.
- `SortedList` containing the best solutions so far.
- `BaseSolution` the last solution found.

In order to use the defined function, it is necessary to compile the configuration section `[ITERATION]`, as below.
```ini
[Iteration]
module name = <python file containing the function> or <library containing the function>
reference name = <name of the iteration function>
# OPTIONAL
module path = <path to the python file directory if module name is not a library>
```

The Iteration Function for the **Peripheral Genes Optimization** is nearly identital to the one for the Core System,
except for the extra `PeripheralWithCoreData` parameter required, containing the data of the Core System used. 
The template is the following

```python
def iter_function(extra: InputExtra) -> Callable:
    # perform initialization operation based on extra data
    def iter_wrapper(data: PeripheralWithCoreData) -> Callable:
        # perform initialization operation based on core data
        def wrapper(factory: NetworkProblemFactory,
                    context: LassimContext,
                    solutions: SortedList,
                    last_solution: PeripheralSolution,
                    prob_init: bool = False
                    ) -> Tuple[Optional[NetworkProblem], SortedDict, bool]:
            if prob_init:
                # initialization of the starting problem to solve
                return start_problem, reactions, False
            else:
                # subsequent iterations
                if perform_a_new_iteration:
                    return new_problem, reactions, True
                else:
                    return None, SortedDict(), False

        return wrapper

    return iter_wrapper
```

A simplified example of the iteration function for the creation of a L1 problem for solving the Core System problem
is presented below.

```python
def iter_l1(extra: InputExtra) -> Callable:
    extra_args = extra.user_dictionary
    lambda_val, step_val, stop_val, repetitions = _get_l1_arguments(extra_args)
    lambdas = _get_lambdas(lambda_val, stop_val, step_val, repetitions)

    # steps for lambda, when to stop, problem creation without edge removal
    def iter_wrapper(factory: CoreProblemFactory, 
                     context: LassimContext,
                     solutions: SortedList, 
                     last_solution: CoreSolution,
                     prob_init: bool = False
                     ) -> Tuple[Optional[CoreProblem], SortedDict, bool]:
                     
        core = context.network.core
        if prob_init:
            reactions_ids = SortedDict(core.from_reactions_to_ids())
            new_lambda = lambdas.pop(0)
            new_problem = factory.build(
                dim=(core.num_tfacts * 2 + core.react_count),
                bounds=default_bounds(core.num_tfacts, core.react_count),
                vector_map=generate_reactions_vector(reactions_ids),
                type="L1",
                lambdas=new_lambda
            )
            return new_problem, reactions_ids, False
        else:
            if len(lambdas) > 0:
                new_lambda = lambdas.pop(0)
                reactions = last_solution.reactions_ids
                red_vect, new_reacts, rem_reacts = remove_under_threshold_reactions(
                        last_solution.solution_vector,
                        reactions,
                        perc_threshold
                    )
                new_react, new_mask = generate_reactions_vector(new_reacts)
                num_react = new_react[new_mask].size
                known_solutions = [red_vect]
                dimension = len(new_reacts.keys()) * 2 + num_react
                new_problem = factory.build(
                    dim=dimension,
                    bounds=default_bounds(len(new_reacts.keys()), num_react),
                    vector_map=(new_react, new_mask),
                    known_sol=known_solutions,
                    type="L1",
                    lambdas=new_lambda
                )
                return new_problem, new_reacts, True
            else:
                return None, SortedDict(), False

    return iter_wrapper
```