import numpy
from setuptools import setup, find_packages, Extension

__author__ = 'Guido Pio Mariotti'
__copyright__ = 'Copyright (C) 2017 Guido Pio Mariotti'
__license__ = 'GNU General Public License v3.0'
__version__ = '0.5.0'

USE_CYTHON = True
try:
    from Cython.Build import cythonize
    from Cython.Distutils import build_ext
except ImportError:
    print("Cython not present, avoiding compiling of cython extensions")
    USE_CYTHON = False

compiler = {}
extensions = None
if USE_CYTHON:
    compiler = {
        'build_ext': build_ext,
        'language_level': 3
    }

    extensions = [
        Extension('lassim.functions.perturbation_functions',
                  ['lassim/functions/perturbation_functions.pyx'],
                  include_dirs=[numpy.get_include()]),
        Extension('lassim.functions.common_functions',
                  ['lassim/functions/common_functions.pyx'],
                  include_dirs=[numpy.get_include()]),
    ]

with open("README.md") as readme:
    long_description = readme.read()

setup(
    name='lassim',
    version=__version__,
    url='https://gitlab.com/Gustafsson-lab/lassim',
    license=__license__,
    author=__author__,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Cython',
        'Programming Language :: Python :: 3.5'
    ],
    description='LASSIM API for GRNs Inference',
    long_description=long_description,
    keywords='lassim grn modeling bioinformatics',
    packages=find_packages(exclude=["tests", "tests.*"]),
    cmdclass=compiler,
    ext_modules=extensions,
    install_requires=[
        'Cython>=0.25.2',
        'matplotlib>=2.0.2',
        'numpy>=1.12.1',
        'pandas>=0.20.2',
        # commented because creates confusion with conda pygmo
        # 'pygmo>=2.4',
        'scipy>=0.19.1',
        'sortedcontainers>=1.5.7'
    ]
)
