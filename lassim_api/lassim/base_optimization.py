import logging
from typing import Callable, Tuple, Optional, Type, List, Dict, Any

import pygmo as pg
from sortedcontainers import SortedDict, SortedList

from .base_solution import BaseSolution
from .lassim_context import LassimContext, OptimizationArgs
from .lassim_exception import LassimException
from .lassim_problem import LassimProblem, LassimProblemFactory
from .solutions_handler import SolutionsHandler
from .type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class BaseOptimization:
    """
    Base class representation of an Optimization. This class uses 
    pygmo.archipelago for performing multiple parallel optimizations using 
    always the same algorithm.
    """

    def __init__(self, algo: Type[pg.algorithm],
                 iter_func: Callable[..., Tuple[Optional[LassimProblem],
                                                SortedDict, bool]]):
        """
        Constructor of BaseOptimization, it only sets the type of algorithm to
        use, the iteration function and the logger. It should be used like a
        factory in conjunction with the build(..) method
        
        :param algo: The reference to a pygmo algorithm.
        :param iter_func: The iteration function to use between each 
            optimization step. If it is set to None, after the first 
            optimization, the solving of the problem will be completed.
        """

        self._algo_type = algo
        # can be None, remember it
        self._iterate = iter_func
        self._logger = logging.getLogger(__name__)

        # these values will be set after a call to build
        # in this way is possible to instantiate this class multiple time
        # without having to change the iteration function and the algorithm
        self._algorithm = None
        self._prob_factory = None
        self._start_problem = None
        self._start_reactions = None
        self._context = None
        self._handler = None
        self._n_evolutions = 0
        self._n_islands = 0
        self._n_individuals = 0

    def build(self,
              context: LassimContext,
              prob_factory: LassimProblemFactory,
              start_problem: LassimProblem,
              reactions: SortedDict,
              handler: SolutionsHandler,
              **kwargs) -> 'BaseOptimization':
        """
        Used for building a BaseOptimization based on the parameter passed in 
        the __init__ call.

        :param context: LassimContext instance. Its OptimizationArgs must
            contain the type of algorithm to use and the parameters needed.
        :param prob_factory: The factory for building new instances of the
            problem. At each iteration, it will be passed as argument to the
            iteration function, if set.
        :param start_problem: The starting problem to solve.
        :param reactions: The dictionary of reactions of this problem.
        :param handler: The SolutionsHandler instance for managing the list of
            solutions found in each iteration.
        :param kwargs: Use kwargs for extra value in extension class.
        :return: The BaseOptimization instance built from the parameter context.
        """

        new_instance = self.__class__(self._algo_type, self._iterate)
        new_instance._context = context
        new_instance._prob_factory = prob_factory
        new_instance._start_problem = start_problem
        new_instance._start_reactions = reactions
        new_instance._handler = handler

        # optimization setup
        opt_args = context.primary_first
        valid_params, verbosity = self.handle_parameters(opt_args)
        new_instance._algorithm = pg.algorithm(
            new_instance._algo_type(**valid_params)
        )
        if new_instance._algorithm.has_set_verbosity():
            new_instance._algorithm.set_verbosity(verbosity)
        new_instance._logger.info(
            "\n" + new_instance._algorithm.get_extra_info()
        )

        # set the archipelago parameters
        new_instance._n_evolutions = opt_args.num_evolutions
        new_instance._n_islands = opt_args.num_islands
        new_instance._n_individuals = opt_args.num_individuals
        return new_instance

    def handle_parameters(self, opt_args: OptimizationArgs
                          ) -> Tuple[Dict[str, Any], int]:
        """
        Evaluates which parameters in the OptimizationArgs instance are valid
        and which are not, based on the algorithm set at instantiation time. 
        Override this method if you want to dynamically change the parameters 
        between each optimization step.

        :param opt_args: An instance of OptimizationArgs for testing the
            parameters.
        :return: The dictionary with the valid parameters for the algorithm.
        """

        # TODO - for now I can't read the signature of __init__
        # valid_params = signature(self._algo_type.__init__).parameters
        # input_params = opt_args.params
        # output_params = {name: input_params[name]
        #                  for name in input_params
        #                  if name in valid_params}
        # return output_params
        params = opt_args.params.copy()
        verbosity = int(params.pop("verbosity", 0))
        try:
            self._algo_type(**params)
            return params, verbosity
        except Exception as e:
            self._logger.error(
                "One or more arguments inside {} are not valid".format(
                    params
                ))
            raise LassimException(e)

    def solve(self, **kwargs) -> SortedList:
        """
        Tries to solve this optimization problem starting from the initial 
        problem passed in the build call. For each optimization step, saves the
        best solution.

        :param kwargs: Extra parameters, to be used for method overriding.
        :return: SortedList of BaseSolution found for this optimization problem,
            ordered by their cost.
        """

        solutions = SortedList()
        try:
            iteration = 1
            solution = self._generate_solution(
                self._start_problem, self._start_reactions
            )
            solutions.add(solution)
            self._logger.info("(I{}) - New solution found with cost {}.".format(
                iteration, solution.cost
            ))

            # check that the iteration function is set
            if self._iterate is not None:
                prob, reactions, do_iteration = self._iterate(
                    self._prob_factory, self._context, solutions, solution
                )
                while do_iteration:
                    # here doesn't change the parameters, but in this way, who
                    # reimplements the class interested only in dynamic
                    # parameters, doesn't have to override the solve method too.
                    valid_params, verbosity = self.handle_parameters(
                        self._context.primary_first
                    )
                    self._algorithm = pg.algorithm(
                        self._algo_type(**valid_params)
                    )
                    if self._algorithm.has_set_verbosity():
                        self._algorithm.set_verbosity(verbosity)
                    self._logger.info("\n" + self._algorithm.get_extra_info())

                    solution = self._generate_solution(prob, reactions)
                    solutions.add(solution)
                    iteration += 1
                    self._logger.info(
                        "(I{}) - New solution found with cost {}.".format(
                            iteration, solution.cost
                        ))
                    prob, reactions, do_iteration = self._iterate(
                        self._prob_factory, self._context, solutions, solution
                    )
            self._logger.info("BaseOptimization completed...")

        except LassimException as e:
            self._logger.error(e)
        except:
            self._logger.exception("Exception occurred during execution...")
        finally:
            self._logger.info("Returning solutions found...")
            return solutions

    def _generate_solution(self,
                           prob: LassimProblem,
                           reactions: SortedDict) -> BaseSolution:
        """
        Creates a new archipelago to solve the problem with the algorithm passed
        as argument, it solveS it, pass the list of solutions to the handler and
        then returns the best solution found.

        :param prob: The LassimProblem instance to solve.
        :param reactions: The dictionary of reactions associated to the problem.
        :return: The best solution found from the solving of the prob instance.
        """

        if self._n_islands > 1:
            archi = self._generate_archipelago(prob)
            self._logger.info("Archipelago generated...")
            archi.evolve(self._n_evolutions)
            archi.wait()
            self._logger.info("Archipelago evolutions completed...")
            solutions = self._get_solutions_from_archipelago(
                archi, prob, reactions
            )
        else:
            pop = self._generate_population(pg.problem(prob), prob.champions)
            for _ in range(self._n_evolutions):
                pop = self._algorithm.evolve(pop)
            self._logger.info("Algorithm evolutions completed...")
            solutions = self._get_solutions_from_population(
                pop, prob, reactions
            )
        self._handler.handle_solutions(solutions)
        self._logger.info("Handled the solutions...")
        return solutions[0]

    def _generate_archipelago(self, prob: LassimProblem) -> pg.archipelago:
        """
        Generates a pygmo.archipelago from the input problem. The algorithm used 
        is the one set at creation time.

        :param prob: The LassimProblem instance to solve.
        :return: The archipelago for solving the LassimProblem.
        """

        pg_problem = pg.problem(prob)
        archi = pg.archipelago(
            n=self._n_islands - 1,
            algo=self._algorithm,
            prob=pg_problem,
            pop_size=self._n_individuals
        )
        self._logger.info("Archipelago created")

        # this population is used for an extra island, containing the previous
        # champions to add to the archipelago
        pop = self._generate_population(pg_problem, prob.champions)
        archi.push_back(algo=self._algorithm, pop=pop)
        self._logger.info("Population pushed")
        return archi

    def _generate_population(self,
                             pg_problem: pg.problem,
                             prev_champions: List[Tuple[Vector, Vector]]
                             ) -> pg.population:
        """
        Generates a population for the input problem, inserting into it 
        previously found champions.
        
        :param pg_problem: pygmo.problem that the population should solve.
        :param prev_champions: List of previously found champions.
        :return: The generated population.
        """

        pop = pg.population(prob=pg_problem, size=self._n_individuals)
        i = 0
        self._logger.info("Population created...")
        while i < self._n_individuals and i < len(prev_champions):
            previous_champion = prev_champions[i]
            pop.set_xf(i, previous_champion[0], previous_champion[1])
            i += 1
        return pop

    def _get_solutions_from_archipelago(self,
                                        archi: pg.archipelago,
                                        prob: LassimProblem,
                                        reactions: SortedDict) -> SortedList:
        """
        From a pygmo.archipelago, generates the list of best solutions found
        by each island, constructing each solution using the class reference
        in the context instance.

        :param archi: The pygmo.archipelago instance from which extracting the
            solutions.
        :param prob: The LassimProblem that has been solved by archi.
        :param reactions: The dictionary of reactions specific for this 
            solution.
        :return SortedList with the solutions equal to the number of islands in
            the archipelago, ordered by their cost.
        """

        # FIXME - two levels of references, it should be avoided
        solution_class = self._context.references.solution_class
        dv_vectors = archi.get_champions_x()
        fitness_vectors = archi.get_champions_f()
        solutions = SortedList(
            [solution_class(dv, fitness, reactions, prob)
             for dv, fitness in zip(dv_vectors, fitness_vectors)]
        )
        return solutions

    def _get_solutions_from_population(self,
                                       pop: pg.population,
                                       prob: LassimProblem,
                                       reactions: SortedDict) -> SortedList:
        """
        From a pygmo.population, generates the best solution found constructing 
        it from the class reference in the context instance.

        :param pop: The pygmo.population containing the solution.
        :param prob: The LassimProblem that has been solved by pop.
        :param reactions: The dictionary of reactions specific for this 
            solution.
        :return SortedList with the solution extracted from the population.
        """

        # FIXME - two levels of references, it should be avoided
        solution_class = self._context.references.solution_class
        solutions = SortedList(
            [solution_class(pop.champion_x, pop.champion_f, reactions, prob)]
        )
        return solutions
