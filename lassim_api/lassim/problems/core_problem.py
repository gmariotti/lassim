from typing import Tuple, Callable, List

import numpy as np

from ..lassim_exception import LassimException
from ..lassim_problem import LassimProblem, LassimProblemFactory
from ..type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class CoreProblem(LassimProblem):
    """
    This class is a representation of an optimization problem for solving a
    core system problem. It is completely independent on how the equations 
    system is designed and solved.
    
    [!] Thread safety must be guaranteed by the ode_function. 
    """

    def __init__(self,
                 dim: int,
                 y0: Vector,
                 bounds: Tuple[Vector, Vector],
                 cost_data: Tuple[Vector, Vector, Vector],
                 map_tuple: Tuple[Vector, Vector],
                 ode_function,
                 known_sol: List[Tuple[Vector, Vector]] = None,
                 **kwargs):
        """
        Constructor of a CoreProblem.

        :param dim: Number of variables defining the problem.
        :param y0: Value of y(t) at time 0 for each equation of the ode system.
        :param bounds: Tuple containing the lower bounds and the upper bounds
            of the problem to solve.
        :param cost_data: The data to use for evaluating the fitness function.
            The tuple must contain the data in this order:
            - measures vector.
            - standard deviation vector.
            - time sequence vector.
        :param map_tuple: Contains a map and its boolean mask as extra 
            parameters for the ode function.
        :param ode_function: The function representing the ODE system to solve.
        :param known_sol: List of known solutions previously found. They seems
            to not help for speeding the optimization process, use them as a
            comparison with the optimization results.
        :param kwargs: Not used.
        """

        super(CoreProblem, self).__init__(dim, y0, map_tuple)

        # for numpy exp overflow
        np.seterr(over="ignore")

        # only arguments that should be public
        if self._vector_map.shape != self._vector_map_mask.shape:
            raise LassimException(
                "Map shape {} is different from mask shape {}".format(
                    self._vector_map.shape, self._vector_map_mask.shape
                ))

        # set parameters for objective function
        self._data, self._sigma, self._time = cost_data

        # check that data and sigma are compatible
        try:
            np.broadcast(self._data, self._sigma)
        except ValueError:
            raise LassimException(
                "Data shape {} is incompatible with sigma shape {}".format(
                    self._data.shape, self._sigma.shape
                ))
        self._sigma2 = np.power(self._sigma, 2)
        self._size = y0.size

        # sets lower bounds and upper bounds
        self.lower_bounds, self.upper_bounds = bounds

        self._ode_function = ode_function

        # these variables are used for performance efficiency
        self._result_mem = np.empty(self._size)
        self._cost_mem = np.empty(self._data.shape)

        # sets the best known solutions
        if known_sol is not None and len(known_sol) > 0:
            self._known_solutions = known_sol

    def fitness(self, dv: Vector) -> Vector:
        """
        Calculates the objective function for this problem with dv as decision
        vector. The function represented is the sum of squares.

        :param dv: Decision vector as numpy.ndarray.
        :return: Numpy.ndarray containing the value of the objective function
            based on the input decision vector.
        """

        solution_vector = np.copy(dv)
        results = self._ode_function(
            self.y0, self._time, solution_vector,
            self._vector_map, self._vector_map_mask, self._size,
            self._result_mem
        )
        norm_results = np.divide(results, np.amax(results, axis=0))
        cost = np.sum(np.divide(np.power(
            np.subtract(self._data, norm_results), 2, out=self._cost_mem),
            self._sigma2, out=self._cost_mem)
        )

        return np.array(cost, ndmin=1)

    def get_bounds(self) -> Tuple[Vector, Vector]:
        """
        Gets the bounds of the problem.
        
        :return: Return a tuple containing the lower bounds and the upper 
            bounds.
        """

        return self.lower_bounds, self.upper_bounds


class CoreWithPerturbationsProblem(CoreProblem):
    """
    This class is an extension of the CoreProblem taking also into account the
    presence of perturbations data.
    
    [!] Thread safety must be guaranteed by ode_function and pert_function.
    """

    def __init__(self,
                 dim: int,
                 y0: Vector,
                 bounds: Tuple[Vector, Vector],
                 cost_data: Tuple[Vector, Vector, Vector],
                 map_tuple: Tuple[Vector, Vector],
                 ode_function,
                 pert_data: Vector,
                 pert_factor: float,
                 pert_function,
                 known_sol: List[Tuple[Vector, Vector]] = None,
                 **kwargs):
        """
        Constructor of a CoreWithPerturbationsProblem.

        :param dim: Number of variables defining the problem.
        :param y0: Value of y(t) at time 0 for each equation of the ode system.
        :param bounds: Tuple containing the lower bounds and the upper bounds
            of the problem to solve.
        :param cost_data: The data to use for evaluating the fitness function.
            The tuple must contain the data in this order:
            - measures vector.
            - standard deviation vector.
            - time sequence vector.
        :param map_tuple: Contains a map and its boolean mask as extra 
            parameters for the ode function.
        :param ode_function: The function representing the ODE system to solve.
        :param pert_data: Vector containing the perturbations data.
        :param pert_factor: Impact of perturbations on the objective function.
        :param pert_function: The function to use for evaluating the impact of
            the perturbations data on the problem.
        :param known_sol: List of known solutions previously found. They seems
            to not help for speeding the optimization process, use them as a
            comparison with the optimization results.
        :param kwargs: Not used.
        """

        # the perturbation data should be already formatted for the perturbation
        # function in order to make the problem as general as possible
        self._pdata = pert_data
        self._factor = pert_factor
        self._pert_function = pert_function
        super(CoreWithPerturbationsProblem, self).__init__(
            dim, y0, bounds, cost_data, map_tuple, ode_function, known_sol
        )

    def fitness(self, dv: Vector) -> Vector:
        """
        Calculates the objective function for this problem with dv as decision
        vector. The function represented is the sum of squares

        :param dv: Decision vector as numpy.ndarray.
        :return: Numpy.ndarray containing the value of the objective function
            based on the input decision vector. The return value is equal to:
            (pert_factor * pert_cost + cost_without_perturbations)
        """

        # cost and pert_cost are assumed independent from each other but running
        # them asynchronously with a pool is slower than doing that sequentially
        cost = super(CoreWithPerturbationsProblem, self).fitness(dv)
        sol_vector = dv.copy()
        pert_cost = self._pert_function(
            self._pdata, self._size, self.y0, sol_vector,
            self._vector_map, self._vector_map_mask, self._ode_function
        )

        return np.array(self._factor * pert_cost + cost, ndmin=1)

    def get_bounds(self) -> Tuple[Vector, Vector]:
        """
        Gets the bounds of the problem.

        :return: Return a tuple containing the lower bounds and the upper 
            bounds.
        """

        return super(CoreWithPerturbationsProblem, self).get_bounds()


class CoreL1Problem(LassimProblem):
    def __init__(self,
                 dim: int,
                 y0: Vector,
                 bounds: Tuple[Vector, Vector],
                 cost_data: Tuple[Vector, Vector, Vector],
                 map_tuple: Tuple[Vector, Vector],
                 ode_function,
                 pert_data: Vector = None,
                 pert_factor: float = None,
                 pert_function=None,
                 known_sol: List[Tuple[Vector, Vector]] = None,
                 **kwargs):
        """
        Constructs a CoreProblem with L1 penalty in the objective function.
        
        :param dim: Number of variables defining the problem.
        :param y0: Value of y(t) at time 0 for each equation of the ode system.
        :param bounds: Tuple containing the lower bounds and the upper bounds
            of the problem to solve.
        :param cost_data: The data to use for evaluating the fitness function.
            The tuple must contain the data in this order:
            - measures vector.
            - standard deviation vector.
            - time sequence vector.
        :param map_tuple: Contains a map and its boolean mask as extra 
            parameters for the ode function.
        :param ode_function: The function representing the ODE system to solve.
        :param pert_data: Vector containing the perturbations data.
        :param pert_factor: Impact of perturbations on the objective function.
        :param pert_function: The function to use for evaluating the impact of
            the perturbations data on the problem.
        :param known_sol: List of known solutions previously found. They seems
            to not help for speeding the optimization process, use them as a
            comparison with the optimization results.
        :param kwargs:
            
            - lambdas: sets the value of lambda for the problem.
            - core_type: name of the core problem to build, between CoreProblem 
                and CoreWithPerturbationsProblem.
        """

        super(CoreL1Problem, self).__init__(dim, y0, map_tuple)
        core_type = kwargs["core_type"]
        if core_type == CoreProblem.__name__:
            self.__core_problem = CoreProblem(
                dim, y0, bounds, cost_data, map_tuple, ode_function, known_sol
            )
        elif core_type == CoreWithPerturbationsProblem.__name__:
            self.__core_problem = CoreWithPerturbationsProblem(
                dim, y0, bounds, cost_data, map_tuple, ode_function,
                pert_data, pert_factor, pert_function, known_sol
            )
        else:
            raise LassimException("{} is not a valid type".format(core_type))
        self.__lambda_val = kwargs["lambdas"]

    def fitness(self, dv: Vector):
        cost = self.__core_problem.fitness(dv)
        if self.lambda_val != 0.0:
            np.add(cost, self.lambda_val * np.sum(np.abs(dv)), cost)
        return cost

    def get_bounds(self) -> Tuple[Vector, Vector]:
        return self.__core_problem.get_bounds()

    @property
    def champions(self) -> List[Tuple[Vector, Vector]]:
        return self.__core_problem.champions

    @property
    def y0(self) -> Vector:
        return super().y0

    @property
    def vector_maps(self) -> Tuple[Vector, Vector]:
        return super().vector_maps

    @property
    def lambda_val(self):
        """
        Value of lambda for the current L1 problem.
        
        :return: L1 lambda value. 
        """

        return self.__lambda_val


class CoreProblemFactory(LassimProblemFactory):
    """
    Factory class for constructing CoreProblem or CoreWithPerturbationsProblem.
    Must be instantiated with a call to new_instance.
    """

    def __init__(self,
                 cost_data: Tuple[Vector, ...],
                 y0: Vector,
                 ode_function: Callable[
                     [Vector, Vector, Vector, Vector, Vector,
                      int, Vector], Vector],
                 pert_function: Callable[
                     [Vector, int, Vector, Vector, Vector, Vector, int,
                      Callable[[Vector, Vector, Vector, Vector, Vector,
                                int, Vector], Vector]], float],
                 pert_factor: float):

        # default problems without perturbations
        self.__problem_dict = {
            "classic": CoreProblem,
            "L1": CoreL1Problem
        }

        self._ode_function = ode_function
        # divides data considering presence or not of perturbations data
        if len(cost_data) == 4:
            self._pert_data = cost_data[3]
            cost_data = (cost_data[0], cost_data[1], cost_data[2])
        self._cost_data = cost_data
        self._y0 = y0
        self.__is_pert = False
        if pert_function is not None:
            self._pert_function = pert_function
            self._pert_factor = pert_factor
            self.__is_pert = True
            self.__problem_dict["classic"] = CoreWithPerturbationsProblem

    @classmethod
    def new_instance(cls,
                     cost_data: Tuple[Vector, ...],
                     y0: Vector,
                     ode_function: Callable[
                         [Vector, Vector, Vector, Vector, Vector,
                          int, Vector], Vector],
                     pert_function: Callable[
                         [Vector, int, Vector, Vector, Vector, Vector, int,
                          Callable[[Vector, Vector, Vector, Vector, Vector,
                                    int, Vector], Vector]], float] = None,
                     pert_factor: float = 0) -> 'CoreProblemFactory':
        """
        Builds a factory for the generation of
        CoreProblem/CoreWithPerturbationsProblem instances with the parameters
        passed as arguments.

        :param cost_data: Tuple containing the vectors for the cost evaluation.
            The first three elements must be the data, the sigma and the time
            sequence. An optional forth value means the presence of
            perturbations data.
        :param y0: Starting values for ODE evaluation.
        :param ode_function: Function for performing the ODE evaluation.
        :param pert_function: Function for evaluate the perturbations impact.
        :param pert_factor: The perturbations factor for perturbations impact.
        :return: Instance of a CoreProblemFactory.
        """

        factory = CoreProblemFactory(
            cost_data, y0, ode_function, pert_function, pert_factor
        )
        return factory

    def build(self,
              dim: int,
              bounds: Tuple[Vector, Vector],
              vector_map: Tuple[Vector, Vector],
              known_sol: List[Tuple[Vector, Vector]] = None,
              **kwargs) -> CoreProblem:
        """
        Constructs a CoreProblem/CoreWithPerturbationsProblem with the 
        parameters passed in the factory instantiation.

        :param dim: Number of variables to optimize for the problem.
        :param bounds: Tuple with the lower bounds and upper bounds of each
            variable represented as numpy.ndarray. Both must have size equal to 
            dim.
        :param vector_map: Tuple containing two vectors needed for the cost
            evaluation. Their values and their representation are independent
            from the problem.
        :param known_sol: List of known solutions for this problem. Useful for
            keeping track of previously found solutions.
        :param kwargs: Optional extra values:
                
            - type: ["classic", "L1"]
            - lambdas: Value of lambda for the L1 problem.
             
            type indicates which kind of problem to build.
        :return: Instance of CoreProblem/CoreWithPerturbationsProblem if type
            is 'classic', CoreL1Problem if type is 'L1'.
        """

        ptype = kwargs.get("type", "classic")

        if not self.__is_pert:
            return self.__problem_dict[ptype](
                dim, np.copy(self._y0),
                (np.copy(bounds[0]), np.copy(bounds[1])),
                (np.copy(self._cost_data[0]),
                 np.copy(self._cost_data[1]),
                 np.copy(self._cost_data[2])),
                (np.copy(vector_map[0]), np.copy(vector_map[1])),
                self._ode_function,
                known_sol=known_sol,
                core_type=CoreProblem.__name__,
                **kwargs
            )
        else:
            return self.__problem_dict[ptype](
                dim, np.copy(self._y0),
                (np.copy(bounds[0]), np.copy(bounds[1])),
                (np.copy(self._cost_data[0]),
                 np.copy(self._cost_data[1]),
                 np.copy(self._cost_data[2])),
                (np.copy(vector_map[0]), np.copy(vector_map[1])),
                self._ode_function,
                np.copy(self._pert_data),
                self._pert_factor,
                self._pert_function,
                known_sol=known_sol,
                core_type=CoreWithPerturbationsProblem.__name__,
                **kwargs
            )
