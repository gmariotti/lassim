from typing import List, Tuple, Callable

import numpy as np

from ..lassim_exception import LassimException
from ..lassim_problem import LassimProblem, LassimProblemFactory
from ..type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class NetworkProblem(LassimProblem):
    """
    This class is a representation of the optimization problem for solving a
    network problem, with a fixed, immutable core system and mutable 
    peripherals. It is completely independent on how the equations system is 
    designed and solved.
    
    [!] Thread safety must be guaranteed by the ode_function.
    """

    def __init__(self,
                 dim: int,
                 y0: Vector,
                 bounds: Tuple[Vector, Vector],
                 cost_data: Tuple[Vector, Vector, Vector],
                 core_data: Tuple[Vector, Vector],
                 map_tuple: Tuple[Vector, Vector],
                 ode_function,
                 known_sol: List[Vector] = None,
                 **kwargs):
        """
        Constructor of a NetworkProblem.

        :param dim: Number of variables defining the problem.
        :param y0: Value of y(t) at time 0 for each equation of the ode system.
        :param bounds: Tuple containing the lower bounds and the upper bounds
            of the problem to solve.
        :param cost_data: The data to use for evaluating the fitness function.
            The tuple must contain the data in this order:
            - measures vector.
            - standard deviation vector.
            - time sequence vector.
        :param core_data: Tuple containing the data of the core system in this
            order:
            - Core System variables.
            - Core System mask.
        :param map_tuple: Contains a map and its boolean mask as extra 
            parameters for the ode function.
        :param ode_function: The function representing the ODE system to solve.
        :param known_sol: List of known solutions previously found. They seems
            to not help for speeding the optimization process, use them as a
            comparison with the optimization results.
        :param kwargs: Not used.
        """

        super(NetworkProblem, self).__init__(dim, y0, map_tuple)

        # for numpy exp overflow
        np.seterr(over="ignore")

        # only arguments that should be public
        if self._vector_map.shape != self._vector_map_mask.shape:
            raise LassimException(
                "Map shape {} is different from mask shape {}".format(
                    self._vector_map.shape, self._vector_map_mask.shape
                ))

        # set parameters for the objective function
        self._net_data, self._sigma, self._time = cost_data

        try:
            np.broadcast(self._net_data, self._sigma)
        except ValueError:
            raise LassimException(
                "Data shape {} is incompatible with sigma shape {}".format(
                    self._net_data.shape, self._sigma.shape
                ))
        self._core_data, self._core_mask = core_data
        if self._core_data.shape != self._core_mask.shape:
            raise LassimException(
                "Core data shape {} is incompatible with mask shape {}".format(
                    self._core_data.shape, self._core_mask.shape
                ))

        self._sigma2 = np.power(self._sigma, 2)
        self._size = y0.size

        # sets the lower bounds and upper bounds
        self.lower_bounds, self.upper_bounds = bounds

        self._ode_function = ode_function

        # these variables are used for performance efficiency
        self._result_mem = np.empty(self._size)
        self._cost_mem = np.empty(self._net_data.shape)
        self._res_pos = self._size - 1

        # save previously found best solutions
        if known_sol is not None:
            self.known_solutions = known_sol

    def fitness(self, dv: Vector) -> Vector:
        """
        Calculates the objective function for this problem with dv as decision
        vector. The function represented is the sum of squares.

        :param dv: Decision vector as numpy.ndarray.
        :return: Numpy.ndarray containing the value of the objective function
            based on the input decision vector.
        """

        # the solution vector represent the variables for the core equations,
        # that are fixed at creation time, plus the variables for the peripheral
        # that varies between _objfun_impl calls.
        solution_vector = self._core_data.copy()
        solution_vector[self._core_mask] = dv.copy()

        results = self._ode_function(
            self._y0, self._time, solution_vector,
            self._vector_map, self._vector_map_mask, self._size,
            self._result_mem
        )
        norm_result = np.divide(
            results[:, self._res_pos], np.amax(results[:, self._res_pos])
        )
        cost = np.sum(np.divide(np.power(
            np.subtract(self._net_data, norm_result), 2, out=self._cost_mem),
            self._sigma2, out=self._cost_mem)
        )
        return np.array(cost, ndmin=1)

    def get_bounds(self) -> Tuple[Vector, Vector]:
        """
        Gets the bounds of the problem.

        :return: Return a tuple containing the lower bounds and the upper 
            bounds.
        """

        return self.lower_bounds, self.upper_bounds


class NetworkWithPerturbationsProblem(NetworkProblem):
    """
    This class is a representation of the optimization problem for solving a
    network problem, with a fixed, immutable lassim_api and mutable peripherals but
    with also perturbations data available.
    It is completely independent on how the equations system is designed and
    solved.
    
    [!] Thread safety must be guaranteed by ode_function and pert_function.
    """

    def __init__(self,
                 dim: int,
                 y0: Vector,
                 bounds: Tuple[Vector, Vector],
                 cost_data: Tuple[Vector, Vector, Vector],
                 core_data: Tuple[Vector, Vector],
                 map_tuple: Tuple[Vector, Vector],
                 ode_function,
                 pert_data: Vector,
                 pert_core: Vector,
                 pert_factor: float,
                 pert_function,
                 known_sol: List[Vector] = None,
                 **kwargs):
        """
        Constructor of NetworkWithPerturbationsProblem.

        :param dim: Number of variables defining the problem.
        :param y0: Value of y(t) at time 0 for each equation of the ode system.
        :param bounds: Tuple containing the lower bounds and the upper bounds
            of the problem to solve.
        :param cost_data: The data to use for evaluating the fitness function.
            The tuple must contain the data in this order:
            - measures vector.
            - standard deviation vector.
            - time sequence vector.
        :param core_data: Tuple containing the data of the core system in this
            order:
            - Core System variables.
            - Core System mask.
        :param map_tuple: Contains a map and its boolean mask as extra 
            parameters for the ode function.
        :param ode_function: The function representing the ODE system to solve.
        :param pert_data: Vector containing the perturbations data.
        :param pert_core: Vector containing the perturbations data of the core
            system.
        :param pert_factor: Impact of perturbations on the objective function.
        :param pert_function: The function to use for evaluating the impact of
            the perturbations data on the problem.
        :param known_sol: List of known solutions previously found. They seems
            to not help for speeding the optimization process, use them as a
            comparison with the optimization results.
        :param kwargs: Not used.
        """

        self._pert_core = pert_core
        self._pert_data = pert_data
        self._pert_factor = pert_factor
        self._pert_function = pert_function
        super(NetworkWithPerturbationsProblem, self).__init__(
            dim, y0, bounds, cost_data, core_data, map_tuple, ode_function,
            known_sol
        )

    def fitness(self, dv: Vector) -> Vector:
        """
        Calculates the objective function for this problem with dv as decision
        vector. The function represented is the sum of squares.

        :param dv: Decision vector as numpy.ndarray.
        :return: Numpy.ndarray containing the value of the objective function
            based on the input decision vector.
        """

        cost = super(NetworkWithPerturbationsProblem, self).fitness(dv)
        core_data = self._core_data.copy(order='F')
        core_data[self._core_mask] = dv.copy()
        sol_vector = np.asfortranarray(core_data)
        pert_cost = self._pert_function(
            self._pert_data, self._size, self._y0, sol_vector,
            self._pert_core, self._vector_map, self._vector_map_mask,
            self._ode_function
        )

        return np.array(self._pert_factor * pert_cost + cost, ndmin=1)

    def get_bounds(self) -> Tuple[Vector, Vector]:
        """
        Gets the bounds of the problem.

        :return: Return a tuple containing the lower bounds and the upper 
            bounds.
        """

        return super().get_bounds()


class NetworkL1Problem(LassimProblem):
    def __init__(self,
                 dim: int,
                 y0: Vector,
                 bounds: Tuple[Vector, Vector],
                 cost_data: Tuple[Vector, Vector, Vector],
                 core_data: Tuple[Vector, Vector],
                 map_tuple: Tuple[Vector, Vector],
                 ode_function,
                 pert_data: Vector = None,
                 pert_core: Vector = None,
                 pert_factor: float = None,
                 pert_function=None,
                 known_sol: List[Vector] = None,
                 **kwargs):
        """
        Constructor of NetworkWithPerturbationsProblem.

        :param dim: Number of variables defining the problem.
        :param y0: Value of y(t) at time 0 for each equation of the ode system.
        :param bounds: Tuple containing the lower bounds and the upper bounds
            of the problem to solve.
        :param cost_data: The data to use for evaluating the fitness function.
            The tuple must contain the data in this order:
            - measures vector.
            - standard deviation vector.
            - time sequence vector.
        :param core_data: Tuple containing the data of the core system in this
            order:
            - Core System variables.
            - Core System mask.
        :param map_tuple: Contains a map and its boolean mask as extra 
            parameters for the ode function.
        :param ode_function: The function representing the ODE system to solve.
        :param pert_data: Vector containing the perturbations data.
        :param pert_core: Vector containing the perturbations data of the core
            system.
        :param pert_factor: Impact of perturbations on the objective function.
        :param pert_function: The function to use for evaluating the impact of
            the perturbations data on the problem.
        :param known_sol: List of known solutions previously found. They seems
            to not help for speeding the optimization process, use them as a
            comparison with the optimization results.
        :param kwargs:
            
            - lambdas: sets the value of lambda for the problem.
            - net_type: name of the network problem to build, between 
                NetworkProblem and NetworkWithPerturbationsProblem.
        """

        super(NetworkL1Problem, self).__init__(dim, y0, map_tuple)
        net_type = kwargs["net_type"]
        if net_type == NetworkProblem.__name__:
            self.__network_problem = NetworkProblem(
                dim, y0, bounds, cost_data, core_data, map_tuple,
                ode_function, known_sol
            )
        elif net_type == NetworkWithPerturbationsProblem.__name__:
            self.__network_problem = NetworkWithPerturbationsProblem(
                dim, y0, bounds, cost_data, core_data, map_tuple, ode_function,
                pert_data, pert_core, pert_factor, pert_function, known_sol
            )
        else:
            raise LassimException("{} is not a valid type".format(net_type))
        self.__lambda_val = kwargs["lambdas"]

    def fitness(self, dv: Vector) -> Vector:
        cost = self.__network_problem.fitness(dv)
        return cost + self.lambda_val * np.sum(np.abs(dv))

    def get_bounds(self) -> Tuple[Vector, Vector]:
        return self.__network_problem.get_bounds()

    @property
    def champions(self) -> List[Tuple[Vector, Vector]]:
        return self.__network_problem.champions

    @property
    def y0(self) -> Vector:
        return super().y0

    @property
    def vector_maps(self) -> Tuple[Vector, Vector]:
        return super().vector_maps

    @property
    def lambda_val(self) -> float:
        """
        Value of lambda for the current L1 problem.

        :return: L1 lambda value. 
        """

        return self.__lambda_val


class NetworkProblemFactory(LassimProblemFactory):
    """
    Factory class for NetworkProblem/NetworkWithPerturbationsProblem.
    Not really useful considering that the problem variables are global one.
    """

    def __init__(self,
                 cost_data: Tuple[Vector, ...],
                 y0: Vector,
                 ode_function: Callable[
                     [Vector, Vector, Vector, Vector, Vector,
                      int, Vector], Vector],
                 pert_function: Callable[
                     [Vector, int, Vector, Vector, Vector, Vector, int,
                      Callable[[Vector, Vector, Vector, Vector, Vector,
                                int, Vector], Vector]], float],
                 pert_factor: float):

        self.__problem_dict = {
            "classic": NetworkProblem,
            "L1": NetworkL1Problem
        }

        self._ode_function = ode_function
        # divides data considering presence or not of perturbations data
        if len(cost_data) == 5:
            self._pert_data = cost_data[3]
            self._pert_core = cost_data[4]
            cost_data = (cost_data[0], cost_data[1], cost_data[2])
        self._cost_data = cost_data
        self._y0 = y0
        self.__is_pert = False
        if pert_function is not None:
            self._pert_function = pert_function
            self._pert_factor = pert_factor
            self.__is_pert = True
            self.__problem_dict["classic"] = NetworkWithPerturbationsProblem

    @classmethod
    def new_instance(cls,
                     cost_data: Tuple[Vector, ...],
                     y0: Vector,
                     ode_function: Callable[
                         [Vector, Vector, Vector, Vector, Vector,
                          int, Vector], Vector],
                     pert_function: Callable[
                         [Vector, int, Vector, Vector, Vector, Vector, int,
                          Callable[[Vector, Vector, Vector, Vector, Vector,
                                    int, Vector], Vector]], float] = None,
                     pert_factor: float = 0) -> 'NetworkProblemFactory':
        """
        Builds a factory for the generation of
        NetworkProblem/NetworkWithPerturbationsProblem instances with the
        parameters passed as arguments:

        :param cost_data: Tuple containing the data for the cost evaluation
            of the problem. The first three elements must be the peripheral
            data, the sigma and the time sequence. An optional forth and fifth
            values means the presence of perturbations data. Forth value is
            the perturbations of the core system, fifth value is the 
            perturbation of the peripheral gene.
        :param y0: Starting values for ODE evaluation.
        :param ode_function: Function for performing the ODE evaluation.
        :param pert_function: Function for evaluate the perturbations impact.
        :param pert_factor: The perturbations factor for perturbations impact.
        :return: Instance of a NetworkProblemFactory.
        """

        factory = NetworkProblemFactory(
            cost_data, y0, ode_function, pert_function, pert_factor
        )
        return factory

    def build(self,
              dim: int,
              bounds: Tuple[Vector, Vector],
              vector_map: Tuple[Vector, Vector],
              known_sol: List[Tuple[Vector, Vector]] = None,
              **kwargs) -> NetworkProblem:
        """
        Construct a NetworkProblem/NetworkWithPerturbationsProblem with the
        parameters passed in the factory instantiation.

        :param dim: Number of variables to optimize for the problem.
        :param bounds: Tuple with the lower bounds and upper bounds of each
            variable represented as numpy.ndarray. Both must have size equal to 
            dim.
        :param vector_map: Tuple containing two vectors needed for the cost
            evaluation. Their values and their representation are independent
            from the problem.
        :param known_sol: List of known solutions for this problem. Useful for
            keeping track of previously found solutions.
        :param kwargs: Extra values, must be present a key named 'core_data'
            containing a tuple of 2 Vector. The second vector must be the mask
            of the first one.
            Optional extra values:
                
            - type: ["classic", "L1"]
            - lambdas: Value of lambda for the L1 problem.
             
            type indicates which kind of problem to build.
        :return: Instance of NetworkProblem/NetworkWithPerturbationsProblem if
            type is 'classic', NetworkL1Problem if type is 'L1'.
        """

        core_data = kwargs.pop("core_data")
        ptype = kwargs.get("type", "classic")

        if not self.__is_pert:
            return self.__problem_dict[ptype](
                dim, np.copy(self._y0),
                (np.copy(bounds[0]), np.copy(bounds[1])),
                (np.copy(self._cost_data[0]),
                 np.copy(self._cost_data[1]),
                 np.copy(self._cost_data[2])),
                (np.copy(core_data[0]), np.copy(core_data[1])),
                (np.copy(vector_map[0]), np.copy(vector_map[1])),
                self._ode_function,
                known_sol=known_sol,
                net_type=NetworkProblem.__name__,
                **kwargs
            )
        else:
            return self.__problem_dict[ptype](
                dim, np.copy(self._y0),
                (np.copy(bounds[0]), np.copy(bounds[1])),
                (np.copy(self._cost_data[0]),
                 np.copy(self._cost_data[1]),
                 np.copy(self._cost_data[2])),
                (np.copy(core_data[0]), np.copy(core_data[1])),
                (np.copy(vector_map[0]), np.copy(vector_map[1])),
                self._ode_function,
                np.copy(self._pert_data),
                np.copy(self._pert_core),
                self._pert_factor,
                self._pert_function,
                known_sol=known_sol,
                net_type=NetworkWithPerturbationsProblem.__name__,
                **kwargs
            )
