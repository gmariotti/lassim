from typing import List

import numpy as np
import pandas as pd
from sortedcontainers import SortedDict

from ..base_solution import BaseSolution
from ..lassim_exception import LassimException
from ..lassim_problem import LassimProblem
from ..problems.core_problem import CoreL1Problem
from ..problems.network_problem import NetworkL1Problem
from ..type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class CoreSolution(BaseSolution):
    """
    Represents the solution of a completed optimization for a CoreProblem.
    """

    def __init__(self,
                 decision_vector: Vector,
                 fitness: Vector,
                 react_ids: SortedDict,
                 prob: LassimProblem):
        """
        Constructor of a CoreSolution.
        
        :param decision_vector: Numpy.ndarray representing the decision vector
            for this solution·
        :param fitness: Numpy.ndarray containing the cost of this solution.
        :param react_ids: Dictionary containing the reactions for this solution.
        :param prob: The instance of the CoreProblem solved. Used to save the
            reaction map and mask, and the y0 vector as copies.
        """

        super().__init__(decision_vector, fitness, react_ids, prob)
        self.react_vect, self.react_mask = prob.vector_maps
        self.y0 = np.copy(prob.y0)

    def get_solution_matrix(self, headers: List[str]) -> pd.DataFrame:
        """
        Creates a pandas.DataFrame representing the solution for a core system
        optimization. Each row of the frame represents a transcription factor,
        while the columns are divided based on the input headers.
        The solution is represented as a matrix containing as column values:
        <y0>,<degradation>,<vmax>,<k_1>,..,<k_n>

        :param headers: List of headers to use as columns.
        :return: The pandas.DataFrame representing the problem's solution.
        """

        num_tfacts = len(self.reactions_ids.keys())
        degradation = np.transpose([self._solution[:num_tfacts]])
        vmax = np.transpose([self._solution[num_tfacts: 2 * num_tfacts]])
        y0 = np.transpose([self.y0])
        react_vect = self.react_vect.copy()
        react_vect[self.react_mask] = self._solution[2 * num_tfacts:]
        react_vect = np.reshape(react_vect, (num_tfacts, num_tfacts))
        degradation_vmax = np.append(degradation, vmax, axis=1)
        matrix = np.append(
            np.append(y0, degradation_vmax, axis=1), react_vect, axis=1
        )
        return pd.DataFrame(data=matrix, columns=headers)

    @classmethod
    def solution_headers(cls) -> List[str]:
        return ["y0", "degradation", "vmax"]


class CoreL1Solution(CoreSolution):
    """
    Represents the solution of a completed optimization for a CoreL1Problem
    """

    def __init__(self,
                 decision_vector: Vector,
                 fitness: Vector,
                 react_ids: SortedDict,
                 prob: CoreL1Problem):
        """
        Constructor of a CoreL1Solution.

        :param decision_vector: Numpy.ndarray representing the decision vector
            for this solution·
        :param fitness: Numpy.ndarray containing the cost of this solution.
        :param react_ids: Dictionary containing the reactions for this solution.
        :param prob: The instance of the CoreL1Problem solved. Used to save the
            reaction map and mask, and the y0 vector as copies.
        """

        super().__init__(decision_vector, fitness, react_ids, prob)
        self._lambda = prob.lambda_val

    def get_solution_matrix(self, headers: List[str]) -> pd.DataFrame:
        """
        Creates a pandas.DataFrame representing the solution for a core system
        optimization. Each row of the frame represents a transcription factor,
        while the columns are divided based on the input headers.
        The solution is represented as a matrix containing as column values:
        <y0>,<lambda>,<degradation>,<vmax>,<k_1>,..,<k_n>

        :param headers: List of headers to use as columns.
        :return: The pandas.DataFrame representing the problem's solution.
        """

        num_tfacts = len(self.reactions_ids.keys())
        y0 = np.transpose([self.y0])
        l1_value = np.transpose(
            [np.array([self._lambda for _ in range(num_tfacts)])]
        )
        degradation = np.transpose([self._solution[:num_tfacts]])
        vmax = np.transpose([self._solution[num_tfacts: 2 * num_tfacts]])
        react_vect = self.react_vect.copy()
        react_vect[self.react_mask] = self._solution[2 * num_tfacts:]
        react_vect = np.reshape(react_vect, (num_tfacts, num_tfacts))
        y0_l1 = np.append(y0, l1_value, axis=1)
        degradation_vmax = np.append(degradation, vmax, axis=1)
        matrix = np.append(
            np.append(y0_l1, degradation_vmax, axis=1), react_vect, axis=1
        )
        return pd.DataFrame(data=matrix, columns=headers)

    @classmethod
    def solution_headers(cls) -> List[str]:
        return ["y0", "l1", "degradation", "vmax"]


class PeripheralSolution(BaseSolution):
    """
    Represents the solution of a completed optimization for a NetworkProblem
    """

    __gene_name = None

    def __init__(self,
                 decision_vector: Vector,
                 fitness: Vector,
                 react_ids: SortedDict,
                 prob: LassimProblem):
        """
        Constructor of a PeripheralSolution.

        :param decision_vector: Numpy.ndarray representing the decision vector
            for this solution·
        :param fitness: Numpy.ndarray containing the cost of this solution.
        :param react_ids: Dictionary containing the reactions for this solution.
        :param prob: The instance of the NetworkProblem solved. Used to save the
            reaction map and mask.
        """

        super().__init__(decision_vector, fitness, react_ids, prob)
        self.react_vect, self.react_mask = prob.vector_maps
        if self.__gene_name is None:
            raise LassimException("Gene name not set!!")
        self._gene = self.__gene_name

    def get_solution_matrix(self, headers: List[str]) -> pd.DataFrame:
        """
        Creates a pandas.DataFrame representing the solution of a peripheral
        optimization. The DataFrame has a single row with the format
        [gene_name, degradation, vmax, k1, .., kn]

        :param headers: Name of the columns to use in the DataFrame.
        :return: pandas.DataFrame representing the problem's solution.
        """

        # the number of transcription factors is equal to the number of columns
        # minus source, lambda and vmax
        num_tfacts = len(headers) - 3

        p_lambda = self.solution_vector[0]
        p_vmax = self.solution_vector[1]
        # each transcription factor has #tfacts + 1 reactions
        tfacts_react_count = (num_tfacts + 1) * num_tfacts
        # -1 because the gene cannot have a reaction with itself
        p_react = self.react_vect[tfacts_react_count:-1].copy()
        p_mask = self.react_mask[tfacts_react_count:-1]
        p_react[p_mask] = self.solution_vector[2:]
        p_lambda_vmax = np.array([self.gene_name, p_lambda, p_vmax])
        matrix = np.append(p_lambda_vmax, p_react)

        return pd.Series(data=matrix, index=headers).to_frame().transpose()

    @property
    def gene_name(self) -> str:
        return self._gene

    @classmethod
    def set_gene_name(cls, name: str):
        cls.__gene_name = name

    @classmethod
    def solution_headers(cls) -> List[str]:
        return ["target", "degradation", "vmax"]


class PeripheralL1Solution(PeripheralSolution):
    """
    Represents the solution of a completed optimization for a NetworkL1Problem
    """

    def __init__(self,
                 decision_vector: Vector,
                 fitness: Vector,
                 react_ids: SortedDict,
                 prob: NetworkL1Problem):
        """
        Constructor of a PeripheralL1Solution

        :param decision_vector: Numpy.ndarray representing the decision vector
            for this solution·
        :param fitness: Numpy.ndarray containing the cost of this solution.
        :param react_ids: Dictionary containing the reactions for this solution.
        :param prob: The instance of the NetworkL1Problem solved. Used to save
            the reaction map and mask.
        """

        super().__init__(decision_vector, fitness, react_ids, prob)
        self._lambda = prob.lambda_val

    def get_solution_matrix(self, headers: List[str]) -> pd.DataFrame:
        """
        Creates a pandas.DataFrame representing the solution of a peripheral
        optimization. The DataFrame has a single row with the format
        [gene_name, l1, degradation, vmax, k1, .., kn]

        :param headers: Name of the columns to use in the DataFrame.
        :return: pandas.DataFrame representing the problem's solution.
        """

        # the number of transcription factors is equal to the number of columns
        # minus source, l1, lambda and vmax
        num_tfacts = len(headers) - 4

        p_lambda = self.solution_vector[0]
        p_vmax = self.solution_vector[1]
        # each transcription factor has #tfacts + 1 reactions
        tfacts_react_count = (num_tfacts + 1) * num_tfacts
        # -1 because the gene cannot have a reaction with itself
        p_react = self.react_vect[tfacts_react_count:-1].copy()
        p_mask = self.react_mask[tfacts_react_count:-1]
        p_react[p_mask] = self.solution_vector[2:]
        p_lassim_specific = np.array(
            [self.gene_name, self._lambda, p_lambda, p_vmax]
        )
        matrix = np.append(p_lassim_specific, p_react)

        return pd.Series(data=matrix, index=headers).to_frame().transpose()

    @property
    def gene_name(self) -> str:
        return self._gene

    @classmethod
    def set_gene_name(cls, name: str):
        super(PeripheralL1Solution, cls).set_gene_name(name)

    @classmethod
    def solution_headers(cls) -> List[str]:
        return ["target", "l1", "degradation", "vmax"]
