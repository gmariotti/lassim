from typing import List, Tuple

from .type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class LassimProblem:
    """
    Interface of a common LASSIM problem. Every problem that are related to
    LASSIM, should implement it and override the fitness and get_bounds methods.
    """

    def __init__(self,
                 dim: int,
                 y0: Vector,
                 vector_maps: Tuple[Vector, Vector],
                 **kwargs):
        self.dimension = dim
        self._y0 = y0
        self._vector_map, self._vector_map_mask = vector_maps
        self._known_solutions = []

    def fitness(self, dv: Vector) -> Vector:
        """
        Calculates the objective function for this problem with dv as decision
        vector.

        :param dv: Decision vector as numpy.ndarray.
        :return: Numpy.ndarray containing the value of the objective function
            based on the input decision vector.
        """

        raise NotImplementedError(self.fitness.__name__)

    def get_bounds(self) -> Tuple[Vector, Vector]:
        """
        Gets the bounds of the problem.
        
        :return: Return a tuple containing the lower bounds and the upper 
            bounds.
        """

        raise NotImplementedError(self.get_bounds.__name__)

    @property
    def champions(self) -> List[Tuple[Vector, Vector]]:
        """
        Property representing the best solutions of the current instance.
        
        :return: List of tuples where, for each tuple, the first element is the
            decision vector and the second one is the value of the objective
            function. Both of them are numpy.ndarray.
        """

        return [(best_x, self.fitness(best_x))
                for best_x in self._known_solutions]

    @property
    def y0(self) -> Vector:
        """
        Property representing the initial values of the problem, y(0).
        
        :return: Copy of the y(0) vector.
        """

        return self._y0.copy()

    @property
    def vector_maps(self) -> Tuple[Vector, Vector]:
        """
        Property representing the map and mask used in the problem.
        
        :return: Copy of map and mask as tuple.
        """

        return self._vector_map.copy(), self._vector_map_mask.copy()


class LassimProblemFactory:
    """
    Interface of a common factory for building LASSIM problems.
    """

    def build(self, dim: int,
              bounds: Tuple[Vector, Vector],
              vector_map: Tuple[Vector, ...],
              known_sol: List[Tuple[Vector, Vector]],
              **kwargs) -> LassimProblem:
        """
        Constructs an instance of a LassimProblem based on the input parameters
        and, optionally, the factory properties.
        
        :param dim: Number of variables to optimize for the problem.
        :param bounds: Tuple with the lower bounds and upper bounds of each
            variable represented as numpy.ndarray. Both must have size equal to 
            dim.
        :param vector_map: Tuple containing two vectors needed for the cost
            evaluation. Their values and their representation are independent
            from the problem.
        :param known_sol: List of known solutions for this problem. Useful for
            keeping track of previously found solutions.
        :param kwargs: Optional extra parameters.
        :return: Instance of a LassimProblemFactory implementing this interface.
        """

        raise NotImplementedError(self.build.__name__)
