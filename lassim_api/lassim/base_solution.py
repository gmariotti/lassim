from copy import deepcopy
from typing import List

import numpy as np
import pandas as pd
from sortedcontainers import SortedDict

from .lassim_problem import LassimProblem
from .type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class BaseSolution:
    """
    Represents the solution of a completed optimization for a LassimProblem.
    """

    def __init__(self,
                 decision_vector: Vector,
                 fitness: Vector,
                 react_ids: SortedDict,
                 prob: LassimProblem):
        """
        Constructor of a BaseSolution.
        
        :param decision_vector: Numpy.ndarray representing the decision vector
            for this solution·
        :param fitness: Numpy.ndarray containing the cost of this solution.
        :param react_ids: Dictionary containing the reactions for this solution.
        :param prob: The instance of the LassimProblem solved.
        """

        self._solution = np.copy(decision_vector)
        self._cost = fitness
        self._reactions_ids = react_ids

    @property
    def cost(self) -> float: return float(self._cost[0])

    @property
    def solution_vector(self) -> Vector:
        return np.copy(self._solution)

    @property
    def number_of_variables(self) -> int: return len(self._solution)

    @property
    def reactions_ids(self) -> SortedDict: return deepcopy(self._reactions_ids)

    @classmethod
    def solution_headers(cls) -> List[str]:
        return list()

    def get_solution_matrix(self, headers: List[str]) -> pd.DataFrame:
        """
        Creates a pandas.DataFrame representation of the solution. Must be
        implemented based on the kind of problem this solution wants to
        represent.

        :param: The list of headers to use for the pandas.DataFrame.
        :return: The pandas.DataFrame representing the solution.
        """

        raise NotImplementedError(self.get_solution_matrix.__name__)

    def __ge__(self, other: 'BaseSolution'): return self.cost >= other.cost

    def __le__(self, other: 'BaseSolution'): return self.cost <= other.cost

    def __eq__(self, other: 'BaseSolution'): return self.cost == other.cost

    def __gt__(self, other: 'BaseSolution'): return self.cost > other.cost

    def __lt__(self, other: 'BaseSolution'): return self.cost < other.cost

    def __ne__(self, other: 'BaseSolution'): return self.cost != other.cost
