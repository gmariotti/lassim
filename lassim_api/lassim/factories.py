from typing import Callable, List, Tuple, Optional

import pygmo as pg
from sortedcontainers import SortedDict

from .base_optimization import BaseOptimization
from .lassim_problem import LassimProblem

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class OptimizationFactory:
    """
    Factory class for getting a BaseOptimization reference based on its name.
    Not part of the BaseOptimization class with the purpose of avoiding any
    circular import, that seems to work/not working for no reason at all.
    """

    # algorithms for solving Continuous Unconstrained Single problems
    _labels_cus = {
        # Heuristic
        "DE": pg.de,
        "JDE": pg.sade,
        "PDE": pg.de1220,
        "PSO": pg.pso,
        "SEA": pg.sea,
        "SA_CORANA": pg.simulated_annealing,
        "BEE_COLONY": pg.bee_colony,
        "CMAES": pg.cmaes,
    }

    # They all have default values and they solve
    @classmethod
    def labels_cus(cls) -> List[str]:
        return sorted(set(cls._labels_cus.keys()))

    @classmethod
    def cus_default(cls) -> str:
        return cls.labels_cus()[0]

    @classmethod
    def new_base_optimization(cls, o_type: str,
                              iter_func: Callable[
                                  ..., Tuple[Optional[LassimProblem],
                                             SortedDict, bool]] = None
                              ) -> BaseOptimization:
        """
        Factory method for creation of a BaseOptimization object.

        :param o_type: The label of an valid optimization algorithm.
        :param iter_func: An optional iteration function to run after each
            completed optimization. Use it in order to dynamically change the
            problem to solve and its reactions.
        :return: An instance of BaseOptimization.
        """

        # raises KeyError if not present
        algo = cls._labels_cus[o_type]
        return BaseOptimization(algo, iter_func)
