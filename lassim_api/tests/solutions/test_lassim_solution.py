from unittest import TestCase
from unittest.mock import create_autospec

import numpy as np
import pandas as pd
from nose.tools import assert_raises, assert_not_equal, assert_equal
from pandas.util.testing import assert_frame_equal
from sortedcontainers import SortedDict, SortedSet, SortedList

from lassim_api.lassim.lassim_exception import LassimException
from lassim_api.lassim.problems.core_problem import CoreL1Problem, CoreProblem
from lassim_api.lassim.problems.network_problem import NetworkProblem, \
    NetworkL1Problem
from lassim_api.lassim.solutions.lassim_solution import CoreSolution, \
    PeripheralSolution, CoreL1Solution, PeripheralL1Solution

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


def create_fake_core_solution(cost: float) -> CoreSolution:
    # first six values are lambdas and vmax
    decision_vector = np.arange(start=0, stop=9)
    fitness = np.array(cost, ndmin=1)
    fake_reactions = SortedDict({
        0: SortedSet(), 1: SortedSet([0, 2]), 2: SortedSet([2])
    })
    mock_core = create_autospec(CoreProblem, instance=True)
    mock_core.vector_maps = (
        np.array([0, 0, 0, 1, 0, 1, 0, 0, 1], dtype=np.float64),
        np.array([False, False, False, True, False, True, False, False, True])
    )
    mock_core.y0 = np.array([0, 0, 0])

    return CoreSolution(decision_vector, fitness, fake_reactions, mock_core)


class TestCoreSolution(TestCase):
    def test_GetSolutionMatrix(self):
        fake_solution = create_fake_core_solution(10)
        headers = CoreSolution.solution_headers() + ["TF1", "TF2", "TF3"]
        expected = pd.DataFrame(
            data=np.array([[0, 0, 3, 0, 0, 0],
                           [0, 1, 4, 6, 0, 7],
                           [0, 2, 5, 0, 0, 8]]),
            columns=headers,
            dtype=np.float64
        )
        actual = fake_solution.get_solution_matrix(headers)
        assert_frame_equal(expected, actual)

    def test_ExceptionForWrongHeaders(self):
        fake_solution = create_fake_core_solution(11)
        headers = ["h1", "h2"]
        assert_raises(ValueError, fake_solution.get_solution_matrix, headers)

    def test_SolutionsCorrectOrder(self):
        fake_solution1 = create_fake_core_solution(10)
        fake_solution2 = create_fake_core_solution(12)
        fake_solution3 = create_fake_core_solution(8)
        solutions = SortedList()
        solutions.add(fake_solution1)
        solutions.add(fake_solution2)
        solutions.add(fake_solution3)

        actual = solutions.pop(1)
        assert_equal(
            actual, fake_solution1,
            "Expected solution with cost {}, but received with cost {}".format(
                fake_solution1.cost, actual.cost
            ))


################################################################################

def create_fake_corel1_solution(cost: float, l1_val: float = 0) -> CoreSolution:
    # first six values are lambdas and vmax
    decision_vector = np.arange(start=0, stop=9)
    fitness = np.array(cost, ndmin=1)
    fake_reactions = SortedDict({
        0: SortedSet(), 1: SortedSet([0, 2]), 2: SortedSet([2])
    })
    mock_core = create_autospec(CoreL1Problem, instance=True)
    mock_core.vector_maps = (
        np.array([0, 0, 0, 1, 0, 1, 0, 0, 1], dtype=np.float64),
        np.array([False, False, False, True, False, True, False, False, True])
    )
    mock_core.y0 = np.array([0, 0, 0])
    mock_core.lambda_val = l1_val

    return CoreL1Solution(decision_vector, fitness, fake_reactions, mock_core)


class TestCoreL1Solution(TestCase):
    def test_GetSolutionMatrix(self):
        fake_solution = create_fake_corel1_solution(10, 5.0)
        headers = CoreL1Solution.solution_headers() + ["TF1", "TF2", "TF3"]
        expected = pd.DataFrame(
            data=np.array([[0, 5.0, 0, 3, 0, 0, 0],
                           [0, 5.0, 1, 4, 6, 0, 7],
                           [0, 5.0, 2, 5, 0, 0, 8]]),
            columns=headers,
            dtype=np.float64
        )
        actual = fake_solution.get_solution_matrix(headers)
        assert_frame_equal(expected, actual)


################################################################################

def create_fake_peripheral_solution(cost: float) -> PeripheralSolution:
    # first two values are lambda and vmax
    decision_vector = np.array([1, 1.5, 10, 21])
    fitness = np.array(cost, ndmin=1)
    # number of transcription factors is 3
    fake_reactions = SortedDict({
        44: SortedSet([0, 2])
    })
    mock_peripheral = create_autospec(NetworkProblem, instance=True)
    mock_peripheral.vector_maps = (
        np.array([10, 15, 1, 0,
                  11, 0, 0, 0,
                  11, 1, 0, 0,
                  1, 0, 1, 0], dtype=np.float64),
        np.array([True, True, True, False,
                  True, False, False, False,
                  True, True, True, True,
                  True, False, True, False])
    )

    return PeripheralSolution(
        decision_vector, fitness, fake_reactions, mock_peripheral
    )


class TestPeripheralSolution(TestCase):
    def setUp(self):
        self.headers = PeripheralSolution.solution_headers() + \
                       ["TF1", "TF2", "TF3"]

    def tearDown(self):
        PeripheralSolution.set_gene_name(None)

    def test_RaisesException(self):
        assert_raises(LassimException, create_fake_peripheral_solution, 10)

    def test_DifferentGenes(self):
        PeripheralSolution.set_gene_name("gene1")
        solution1 = create_fake_peripheral_solution(10)
        PeripheralSolution.set_gene_name("gene2")
        solution2 = create_fake_peripheral_solution(10)
        assert_not_equal(solution1.gene_name, solution2.gene_name,
                         "Expected different gene name but received the same")

    def test_SameGene(self):
        PeripheralSolution.set_gene_name("gene")
        solution1 = create_fake_peripheral_solution(10)
        solution2 = create_fake_peripheral_solution(11)
        assert_equal(solution1.gene_name, solution2.gene_name,
                     "Expected same gene name but received a different one")

    def test_SolutionMatrix(self):
        PeripheralSolution.set_gene_name("gene")
        solution = create_fake_peripheral_solution(10)
        data = np.array(["gene", 1.0, 1.5, 10.0, 0.0, 21.0])
        expected = pd.Series(data, self.headers).to_frame().transpose()
        actual = solution.get_solution_matrix(self.headers)
        assert_frame_equal(expected, actual)

    def test_JoinMultipleSolutions(self):
        PeripheralSolution.set_gene_name("gene1")
        solution1 = create_fake_peripheral_solution(10)
        PeripheralSolution.set_gene_name("gene2")
        solution2 = create_fake_peripheral_solution(10)
        PeripheralSolution.set_gene_name("gene3")
        solution3 = create_fake_peripheral_solution(10)

        expected = pd.DataFrame(
            data=np.array([["gene1", 1.0, 1.5, 10.0, 0.0, 21.0],
                           ["gene2", 1.0, 1.5, 10.0, 0.0, 21.0],
                           ["gene3", 1.0, 1.5, 10.0, 0.0, 21.0]]),
            columns=self.headers
        )
        actual = pd.concat([solution1.get_solution_matrix(self.headers),
                            solution2.get_solution_matrix(self.headers),
                            solution3.get_solution_matrix(self.headers)],
                           ignore_index=True)
        assert_frame_equal(expected, actual)


################################################################################

def create_fake_peripheralL1_solution(cost: float) -> PeripheralL1Solution:
    # first two values are lambda and vmax
    decision_vector = np.array([1, 1.5, 10, 21])
    fitness = np.array(cost, ndmin=1)
    # number of transcription factors is 3
    fake_reactions = SortedDict({
        44: SortedSet([0, 2])
    })
    mock_peripheral = create_autospec(NetworkL1Problem, instance=True)
    mock_peripheral.lambda_val = 10.0
    mock_peripheral.vector_maps = (
        np.array([10, 15, 1, 0,
                  11, 0, 0, 0,
                  11, 1, 0, 0,
                  1, 0, 1, 0], dtype=np.float64),
        np.array([True, True, True, False,
                  True, False, False, False,
                  True, True, True, True,
                  True, False, True, False])
    )

    return PeripheralL1Solution(
        decision_vector, fitness, fake_reactions, mock_peripheral
    )


class TestPeripheralL1Solution(TestCase):
    def setUp(self):
        self.headers = PeripheralL1Solution.solution_headers() + [
            "TF1", "TF2", "TF3"
        ]

    def tearDown(self):
        PeripheralL1Solution.set_gene_name(None)

    def test_SolutionMatrix(self):
        PeripheralL1Solution.set_gene_name("gene")
        solution = create_fake_peripheralL1_solution(10)
        data = np.array(["gene", 10.0, 1.0, 1.5, 10.0, 0.0, 21.0])
        expected = pd.Series(data, self.headers).to_frame().transpose()
        actual = solution.get_solution_matrix(self.headers)
        assert_frame_equal(expected, actual)
