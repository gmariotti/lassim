LASSIM Toolbox
==============

About LASSIM and the LASSIM toolbox
-----------------------------------

Recent and ongoing improvements in measurements technologies have given the possibility 
to obtain systems wide omics data of several biological processes. However, the analysis of 
those data has to date been restricted to crude, statistical tools with important biological 
mechanisms, e.g. feedback loops, being overlooked. The omitting of such high influence details 
in a large scale network remains a major problem in today’s omics based environment and is a 
key aspect of truly understanding any complex disease. Therefore, we herein present the 
**LASSIM** (**LA**rge **S**cale **SI**mulation **M**odeling) toolbox for GRNs Inference, which revolves around the 
expansion of a well determined mechanistic ODE-model into the entire system.

With this toolbox, it is possible to run a default implementation of `lassim`, but also to
extend and improve its behaviour by creating new optimization algorithms, using a different 
system of ODEs, different types of integrators, and much more.

All the optimization algorithms currently available are implemented by [pygmo](https://esa.github.io/pagmo2/) but there 
are no limitations on how the algorithms should be implemented, what it is important is to respect
the signature of the classes that are part of the module `lassim_api`

**[Important]**
Since version 1.0, the `lassim_toolbox` is considered stable enough to be used in day to day operations. However, 
the `lassim_api` is still in the beta release stage. For this reason, if you are planning to just use the toolbox,
everything should work as expected, but if you are planning of integrating the `lassim_api` in an existing project,
keep in mind that the general structure can be still subject to important changes. If you have any feedback or you 
want to contribute to the project, don't esitate to contact the current developers of it.

Current Members in the Project
------------------------------
- @gmariotti
- @rasma87

How to install the toolbox
----------------------------------
Before using the the toolbox, be sure to satisfy all the requirements in the [Development environment and requirements](#development-environment-and-requirements). 
After you have done that, run the following commands from a terminal:
```
git clone https://gitlab.com/Gustafsson-lab/lassim.git
cd lassim/lassim_api
pip uninstall lassim -y
pip install .
```

In order to run the tests in the repository code, the following code should be executed from 
the `lassim` folder:
```bash
nosetests lassim_api/
export PYTHONPATH="$PYTHONPATH:$(pwd)/lassim_toolbox/source/"
nosetests lassim_toolbox
```

How to use the toolbox
----------------------
For the **Core System Optimization**, the command is:
```bash
python lassim_core.py <configuration-file>
```
While for the **Peripheral Genes Optimization**, the command is:
```bash
python lassim_peripherals.py <configuration-file>
```
or for running the **Peripheral Genes Optimization** using MPI:
```bash
mpiexec -n <n> python lassim_peripherals_mpi.py <configuration-file>
```

while for the list of terminal options available, use the command:
- `python lassim_core.py -h`
- `python lassim_peripherals.py -h`

Cytoscape Integration
---------------------
An initial version of [Cytoscape](http://www.cytoscape.org/) integration with **LASSIM**'s solutions 
is available using the script `lassim_cytoscape.py`. Currently, can be safely used to generate a network 
from a **Core System** optimization with a custom style to represent it. In order to run the script, 
it is necessary a custom version of [py2cytoscape](https://github.com/idekerlab/py2cytoscape) that 
can be downloaded at [py2cytoscape_gmariotti](https://github.com/gmariotti/py2cytoscape).

Use command `python lassim_cytoscape.py -h` for the available options in the script.

Development environment and requirements
----------------------------------------
The current environment used for development and testing is:

- [Fedora 25 Workstation](https://getfedora.org/)
- [PyCharm 2016.x](https://www.jetbrains.com/pycharm/)
- [Anaconda 4.1.1](https://anaconda.org/) with Python 3.5.2

but it is used mainly on the [NSC](https://www.nsc.liu.se) system available at the [Linköping University](http://liu.se/?l=en).

Instead, the list of **mandatory** dependencies is:

- [pygmo](https://esa.github.io/pagmo2/)
- [matplotlib](https://matplotlib.org/)
- [NumPy/SciPy](http://www.scipy.org/)
- [pandas](http://pandas.pydata.org/)
- [Cython](http://cython.org/)
- [sortedcontainers](http://www.grantjenks.com/docs/sortedcontainers/)

Except for [sortedcontainers](http://www.grantjenks.com/docs/sortedcontainers/) and [pygmo](https://esa.github.io/pagmo2/), 
all of them are already present in [Anaconda 4.1.1](https://anaconda.org/).

Check [INSTALL.md](INSTALL.md) for how to install the necessary dependencies.

**[!]** Windows OS is not supported yet.

Python Version Supported/Tested
-------------------------------
- Python 3.6
- Python 3.5

Examples and Documentation
--------------------------
The following examples are currently available for the toolbox:

- `examples/core_system` contains an example for the Core System Optimization, with configuration files
for problem with or without perturbations, or with the L1 penalty.
- `examples/peripheral_genes` contains an example for the Peripheral Genes Optimization, with configuration 
files for problem with or without perturbations, or with the L1 penalty.

In folder `docs` are present some `HowTo` guides on how to create custom functions for `lassim`, while in the
`resources` folder there are images on how `lassim_core` and `lassim_peripherals` algorithms are implemented
in the toolbox.

What's next?
------------

- Making `lassim_api` a Python module installable from **PyPI**.
- New kind of base implementation for the optimization process, in order to use different algorithm 
in different ways.
- New formats for input data.
- Improvements on tests, documentation and code quality.
- A Gitter channel.

[Here](https://python3statement.github.io/) you can find one of the reasons why the support for Python 2.7 is highly improbable.

Current Branches
----------------

The `master` branch, usually, contains working code, tested on different environments. Check `releases` to see the 
latest stable version of the toolbox.

The `development` branch, instead, contains unstable, untested code, with future features and bug fixes. It should not 
be used unless you want to help with the development. 

How to contribute
-----------------
In order to contribute to the project, it is necessary to first `fork` it. Then, it is important to decide on which branch 
to work on:
- `master` branch accepts only bug fixes and small corrections to the current code.
- `development` branch accepts bug fixes, corrections and the development of new features that will become part of
the `master` branch.

In all cases, for each pull request, it would be nice to have some tests related to the code submitted and an explanation
of what it is submitted, in order to make the job easier for the repository maintainers.

References
----------
> [**LASSIM—A network inference toolbox for genome-wide mechanistic modeling**](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005608), 
Magnusson Rasmus and Mariotti Guido Pio and others, _PLoS Computational Biology_, 13(6)

> [**The generalized island model**](https://link.springer.com/chapter/10.1007%2F978-3-642-28789-3_7), 
Izzo Dario and Ruci&#324;ski Marek and Biscani Francesco, _Parallel Architectures and Bioinspired Algorithms_, 151--169, 2012, Springer 
